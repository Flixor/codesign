EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SemiparamEQ1-rescue:LM13700-Amplifier_Operational U?
U 1 1 5DBC645F
P 6550 2600
AR Path="/5DBC645F" Ref="U?"  Part="1" 
AR Path="/5DB4A226/5DBC645F" Ref="U7"  Part="1" 
F 0 "U7" H 6550 2967 50  0000 C CNN
F 1 "LM13700" H 6550 2876 50  0000 C CNN
F 2 "Codesign:DIP-16_W6.35mm" H 6250 2625 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm13700.pdf" H 6250 2625 50  0001 C CNN
F 4 "x" H 6550 2600 50  0001 C CNN "onderdelen 5mar20"
	1    6550 2600
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM13700-Amplifier_Operational U?
U 3 1 5DBC6465
P 5450 2600
AR Path="/5DBC6465" Ref="U?"  Part="3" 
AR Path="/5DB4A226/5DBC6465" Ref="U7"  Part="3" 
F 0 "U7" H 5450 2967 50  0000 C CNN
F 1 "LM13700" H 5450 2876 50  0000 C CNN
F 2 "Codesign:DIP-16_W6.35mm" H 5150 2625 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm13700.pdf" H 5150 2625 50  0001 C CNN
F 4 "x" H 5450 2600 50  0001 C CNN "onderdelen 5mar20"
	3    5450 2600
	-1   0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM13700-Amplifier_Operational U?
U 4 1 5DBC646B
P 4450 2650
AR Path="/5DBC646B" Ref="U?"  Part="4" 
AR Path="/5DB4A226/5DBC646B" Ref="U7"  Part="4" 
F 0 "U7" H 4350 2998 50  0000 C CNN
F 1 "LM13700" H 4350 2907 50  0000 C CNN
F 2 "Codesign:DIP-16_W6.35mm" H 4150 2675 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm13700.pdf" H 4150 2675 50  0001 C CNN
F 4 "x" H 4450 2650 50  0001 C CNN "onderdelen 5mar20"
	4    4450 2650
	-1   0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM13700-Amplifier_Operational U?
U 2 1 5DBC6471
P 7500 2650
AR Path="/5DBC6471" Ref="U?"  Part="2" 
AR Path="/5DB4A226/5DBC6471" Ref="U7"  Part="2" 
F 0 "U7" H 7400 2998 50  0000 C CNN
F 1 "LM13700" H 7400 2907 50  0000 C CNN
F 2 "Codesign:DIP-16_W6.35mm" H 7200 2675 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm13700.pdf" H 7200 2675 50  0001 C CNN
F 4 "x" H 7500 2650 50  0001 C CNN "onderdelen 5mar20"
	2    7500 2650
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6477
P 5300 1900
AR Path="/5DBC6477" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6477" Ref="R29"  Part="1" 
F 0 "R29" V 5507 1900 50  0000 C CNN
F 1 "100k" V 5416 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5230 1900 50  0001 C CNN
F 3 "~" H 5300 1900 50  0001 C CNN
F 4 "x" H 5300 1900 50  0001 C CNN "onderdelen 5mar20"
	1    5300 1900
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC647D
P 6650 1900
AR Path="/5DBC647D" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC647D" Ref="R35"  Part="1" 
F 0 "R35" V 6857 1900 50  0000 C CNN
F 1 "100k" V 6766 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6580 1900 50  0001 C CNN
F 3 "~" H 6650 1900 50  0001 C CNN
F 4 "x" H 6650 1900 50  0001 C CNN "onderdelen 5mar20"
	1    6650 1900
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6483
P 6000 2250
AR Path="/5DBC6483" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6483" Ref="R32"  Part="1" 
F 0 "R32" V 6207 2250 50  0000 C CNN
F 1 "1k" V 6116 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5930 2250 50  0001 C CNN
F 3 "~" H 6000 2250 50  0001 C CNN
F 4 "x" H 6000 2250 50  0001 C CNN "onderdelen 5mar20"
	1    6000 2250
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6489
P 7750 3100
AR Path="/5DBC6489" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6489" Ref="R36"  Part="1" 
F 0 "R36" H 7820 3146 50  0000 L CNN
F 1 "10k" H 7820 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7680 3100 50  0001 C CNN
F 3 "~" H 7750 3100 50  0001 C CNN
F 4 "x" H 7750 3100 50  0001 C CNN "onderdelen 5mar20"
	1    7750 3100
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC648F
P 4200 3050
AR Path="/5DBC648F" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC648F" Ref="R24"  Part="1" 
F 0 "R24" H 4270 3096 50  0000 L CNN
F 1 "10k" H 4270 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4130 3050 50  0001 C CNN
F 3 "~" H 4200 3050 50  0001 C CNN
F 4 "x" H 4200 3050 50  0001 C CNN "onderdelen 5mar20"
	1    4200 3050
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6495
P 5750 3150
AR Path="/5DBC6495" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6495" Ref="R30"  Part="1" 
F 0 "R30" H 5820 3196 50  0000 L CNN
F 1 "3k" H 5820 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5680 3150 50  0001 C CNN
F 3 "~" H 5750 3150 50  0001 C CNN
F 4 "x" H 5750 3150 50  0001 C CNN "onderdelen 5mar20"
	1    5750 3150
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC649B
P 6000 3150
AR Path="/5DBC649B" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC649B" Ref="R33"  Part="1" 
F 0 "R33" H 6070 3196 50  0000 L CNN
F 1 "72k" H 6070 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5930 3150 50  0001 C CNN
F 3 "~" H 6000 3150 50  0001 C CNN
F 4 "x" H 6000 3150 50  0001 C CNN "onderdelen 5mar20"
	1    6000 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2600 6050 2600
Wire Wire Line
	6050 2600 6050 2700
Wire Wire Line
	6050 2700 6250 2700
Wire Wire Line
	5850 2250 5850 2600
Wire Wire Line
	5750 2700 5950 2700
Wire Wire Line
	5950 2700 5950 2400
Wire Wire Line
	5950 2400 6150 2400
Wire Wire Line
	6150 2400 6150 2250
Wire Wire Line
	5750 2500 5750 2250
Wire Wire Line
	5750 2250 5850 2250
Connection ~ 5850 2250
Wire Wire Line
	6250 2500 6250 2250
Wire Wire Line
	6250 2250 6150 2250
Connection ~ 6150 2250
Wire Wire Line
	6250 2250 6250 1900
Wire Wire Line
	6250 1900 6500 1900
Connection ~ 6250 2250
Wire Wire Line
	6800 1900 7750 1900
Wire Wire Line
	7750 1900 7750 2750
Wire Wire Line
	7600 2750 7750 2750
Connection ~ 7750 2750
Wire Wire Line
	7750 2750 7750 2950
Wire Wire Line
	5750 2250 5750 1900
Wire Wire Line
	5750 1900 5450 1900
Connection ~ 5750 2250
Wire Wire Line
	5150 1900 4200 1900
Wire Wire Line
	4200 1900 4200 2750
Wire Wire Line
	4350 2750 4200 2750
Connection ~ 4200 2750
Wire Wire Line
	4200 2750 4200 2900
Wire Wire Line
	5150 3000 5150 2700
Wire Wire Line
	6000 3000 6850 3000
Wire Wire Line
	6850 3000 6850 2700
Connection ~ 6000 3000
Connection ~ 5750 3000
Wire Wire Line
	5750 3000 5150 3000
Wire Wire Line
	5750 3000 6000 3000
Wire Wire Line
	5750 3300 5900 3300
Wire Wire Line
	5150 2600 4950 2600
Wire Wire Line
	4950 2600 4950 2650
Wire Wire Line
	4950 2650 4900 2650
Wire Wire Line
	7050 2600 7050 2650
Wire Wire Line
	7050 2650 7200 2650
Wire Wire Line
	6850 2600 7050 2600
Wire Wire Line
	5450 3600 5450 3400
Wire Wire Line
	5450 3400 5150 3400
Wire Wire Line
	4900 3400 4900 2650
Connection ~ 4900 2650
Wire Wire Line
	4900 2650 4750 2650
Text Notes 4200 1550 0    50   ~ 0
Floating voltage controlled R\nFig 28 datasheet (yes, pins 2 & 15 are unconnected!)\nRx = 2RRc/(19.2VcRa)\nVabc=0-3V, Rx=30k at Vc=1V
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC64E9
P 1600 5500
AR Path="/5DBC64E9" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC64E9" Ref="R18"  Part="1" 
F 0 "R18" H 1670 5546 50  0000 L CNN
F 1 "40k" H 1670 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1530 5500 50  0001 C CNN
F 3 "~" H 1600 5500 50  0001 C CNN
F 4 "x" H 1600 5500 50  0001 C CNN "onderdelen 5mar20"
	1    1600 5500
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR?
U 1 1 5DBC64F5
P 1600 5350
AR Path="/5DBC64F5" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DBC64F5" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 1600 5200 50  0001 C CNN
F 1 "+5V" H 1615 5523 50  0000 C CNN
F 2 "" H 1600 5350 50  0001 C CNN
F 3 "" H 1600 5350 50  0001 C CNN
	1    1600 5350
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DBC64FB
P 1600 6100
AR Path="/5DBC64FB" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DBC64FB" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 1600 5850 50  0001 C CNN
F 1 "GND" H 1605 5927 50  0000 C CNN
F 2 "" H 1600 6100 50  0001 C CNN
F 3 "" H 1600 6100 50  0001 C CNN
	1    1600 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 5650 1600 5750
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6508
P 2200 5750
AR Path="/5DBC6508" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6508" Ref="R20"  Part="1" 
F 0 "R20" V 1993 5750 50  0000 C CNN
F 1 "500k" V 2084 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2130 5750 50  0001 C CNN
F 3 "~" H 2200 5750 50  0001 C CNN
F 4 "x" H 2200 5750 50  0001 C CNN "onderdelen 5mar20"
	1    2200 5750
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC650E
P 2900 5300
AR Path="/5DBC650E" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC650E" Ref="R21"  Part="1" 
F 0 "R21" V 2693 5300 50  0000 C CNN
F 1 "500k" V 2784 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2830 5300 50  0001 C CNN
F 3 "~" H 2900 5300 50  0001 C CNN
F 4 "x" H 2900 5300 50  0001 C CNN "onderdelen 5mar20"
	1    2900 5300
	0    1    1    0   
$EndComp
Connection ~ 1600 5750
Wire Wire Line
	1600 5750 1600 5800
Wire Wire Line
	3200 5850 3200 5650
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DBC651D
P 2500 6000
AR Path="/5DBC651D" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DBC651D" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 2500 5750 50  0001 C CNN
F 1 "GND" H 2505 5827 50  0000 C CNN
F 2 "" H 2500 6000 50  0001 C CNN
F 3 "" H 2500 6000 50  0001 C CNN
	1    2500 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 6000 2500 5950
Wire Wire Line
	2500 5950 2600 5950
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC652B
P 5900 5750
AR Path="/5DBC652B" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC652B" Ref="R31"  Part="1" 
F 0 "R31" V 5693 5750 50  0000 C CNN
F 1 "100k" V 5784 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5830 5750 50  0001 C CNN
F 3 "~" H 5900 5750 50  0001 C CNN
F 4 "x" H 5900 5750 50  0001 C CNN "onderdelen 5mar20"
	1    5900 5750
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6531
P 6400 5750
AR Path="/5DBC6531" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6531" Ref="R34"  Part="1" 
F 0 "R34" V 6193 5750 50  0000 C CNN
F 1 "100k" V 6284 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 5750 50  0001 C CNN
F 3 "~" H 6400 5750 50  0001 C CNN
F 4 "x" H 6400 5750 50  0001 C CNN "onderdelen 5mar20"
	1    6400 5750
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC6537
P 4600 5650
AR Path="/5DBC6537" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC6537" Ref="R27"  Part="1" 
F 0 "R27" V 4393 5650 50  0000 C CNN
F 1 "10k" V 4484 5650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4530 5650 50  0001 C CNN
F 3 "~" H 4600 5650 50  0001 C CNN
F 4 "x" H 4600 5650 50  0001 C CNN "onderdelen 5mar20"
	1    4600 5650
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC653D
P 4800 5100
AR Path="/5DBC653D" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC653D" Ref="R28"  Part="1" 
F 0 "R28" H 4650 5100 50  0000 C CNN
F 1 "10k" H 4650 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4730 5100 50  0001 C CNN
F 3 "~" H 4800 5100 50  0001 C CNN
F 4 "x" H 4800 5100 50  0001 C CNN "onderdelen 5mar20"
	1    4800 5100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 5650 4800 5650
Wire Wire Line
	6050 5750 6150 5750
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DBC654C
P 6600 5900
AR Path="/5DBC654C" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DBC654C" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 6600 5650 50  0001 C CNN
F 1 "GND" H 6605 5727 50  0000 C CNN
F 2 "" H 6600 5900 50  0001 C CNN
F 3 "" H 6600 5900 50  0001 C CNN
	1    6600 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5900 6600 5750
Wire Wire Line
	6600 5750 6550 5750
Wire Wire Line
	6150 5750 6150 6050
Wire Wire Line
	6150 6050 4900 6050
Wire Wire Line
	4900 6050 4900 5850
Connection ~ 6150 5750
Wire Wire Line
	6150 5750 6250 5750
$Comp
L SemiparamEQ1-rescue:LM13700-Amplifier_Operational U?
U 5 1 5DBDDF46
P 8800 2500
AR Path="/5DBDDF46" Ref="U?"  Part="5" 
AR Path="/5DB4A226/5DBDDF46" Ref="U7"  Part="5" 
F 0 "U7" H 8758 2546 50  0000 L CNN
F 1 "LM13700" H 8758 2455 50  0000 L CNN
F 2 "Codesign:DIP-16_W6.35mm" H 8500 2525 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm13700.pdf" H 8500 2525 50  0001 C CNN
F 4 "x" H 8800 2500 50  0001 C CNN "onderdelen 5mar20"
	5    8800 2500
	1    0    0    -1  
$EndComp
Text HLabel 5450 3600 0    50   Output ~ 0
Rx_a
Wire Wire Line
	7050 2650 7050 3400
Wire Wire Line
	7050 3400 6600 3400
Connection ~ 7050 2650
Connection ~ 5900 3300
Wire Wire Line
	5900 3300 6000 3300
Text Label 5900 3500 2    50   ~ 0
Vabc
Text Label 5500 3000 2    50   ~ 0
Iabc
$Comp
L SemiparamEQ1-rescue:C-Device C13
U 1 1 5DCA335A
P 1500 2800
F 0 "C13" V 1248 2800 50  0000 C CNN
F 1 "100n" V 1339 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1538 2650 50  0001 C CNN
F 3 "~" H 1500 2800 50  0001 C CNN
F 4 "x" H 1500 2800 50  0001 C CNN "onderdelen 5mar20"
	1    1500 2800
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C16
U 1 1 5DCB7CC3
P 8500 2800
F 0 "C16" V 8248 2800 50  0000 C CNN
F 1 "100n" V 8339 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8538 2650 50  0001 C CNN
F 3 "~" H 8500 2800 50  0001 C CNN
F 4 "x" H 8500 2800 50  0001 C CNN "onderdelen 5mar20"
	1    8500 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 2200 8650 2200
Wire Wire Line
	8700 2800 8650 2800
$Comp
L SemiparamEQ1-rescue:C-Device C12
U 1 1 5DCF19DF
P 1500 2200
F 0 "C12" V 1248 2200 50  0000 C CNN
F 1 "100n" V 1339 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1538 2050 50  0001 C CNN
F 3 "~" H 1500 2200 50  0001 C CNN
F 4 "x" H 1500 2200 50  0001 C CNN "onderdelen 5mar20"
	1    1500 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 2200 1750 2200
Wire Wire Line
	1750 2800 1650 2800
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DCF704E
P 1250 2450
AR Path="/5DCF704E" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DCF704E" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 1250 2200 50  0001 C CNN
F 1 "GND" H 1255 2277 50  0000 C CNN
F 2 "" H 1250 2450 50  0001 C CNN
F 3 "" H 1250 2450 50  0001 C CNN
	1    1250 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2200 1350 2450
Wire Wire Line
	1250 2450 1350 2450
Connection ~ 1350 2450
Wire Wire Line
	1350 2450 1350 2800
Wire Wire Line
	8350 2800 8300 2800
$Comp
L SemiparamEQ1-rescue:C-Device C15
U 1 1 5DD041E3
P 8500 2200
F 0 "C15" V 8248 2200 50  0000 C CNN
F 1 "100n" V 8339 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8538 2050 50  0001 C CNN
F 3 "~" H 8500 2200 50  0001 C CNN
F 4 "x" H 8500 2200 50  0001 C CNN "onderdelen 5mar20"
	1    8500 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 2200 8300 2200
$Comp
L SemiparamEQ1-rescue:GND-power #PWR032
U 1 1 5DD044FF
P 8200 2450
F 0 "#PWR032" H 8200 2200 50  0001 C CNN
F 1 "GND" H 8205 2277 50  0000 C CNN
F 2 "" H 8200 2450 50  0001 C CNN
F 3 "" H 8200 2450 50  0001 C CNN
	1    8200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 2200 8300 2450
Wire Wire Line
	8200 2450 8300 2450
Connection ~ 8300 2450
Wire Wire Line
	8300 2450 8300 2800
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP5
U 1 1 5DE980DC
P 6250 4750
F 0 "TP5" V 6445 4822 50  0000 C CNN
F 1 "Vabc" V 6354 4822 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 6450 4750 50  0001 C CNN
F 3 "~" H 6450 4750 50  0001 C CNN
F 4 "x" H 6250 4750 50  0001 C CNN "onderdelen 5mar20"
	1    6250 4750
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP4
U 1 1 5DE9BD0C
P 5150 3350
F 0 "TP4" H 5208 3468 50  0000 L CNN
F 1 "Rfloat_a" H 5208 3377 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 5350 3350 50  0001 C CNN
F 3 "~" H 5350 3350 50  0001 C CNN
F 4 "x" H 5150 3350 50  0001 C CNN "onderdelen 5mar20"
	1    5150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3350 5150 3400
Connection ~ 5150 3400
Wire Wire Line
	5150 3400 4900 3400
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP6
U 1 1 5DE9F662
P 6600 3350
F 0 "TP6" H 6658 3468 50  0000 L CNN
F 1 "Rfloat_b" H 6658 3377 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 6800 3350 50  0001 C CNN
F 3 "~" H 6800 3350 50  0001 C CNN
F 4 "x" H 6600 3350 50  0001 C CNN "onderdelen 5mar20"
	1    6600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3350 6600 3400
Connection ~ 6600 3400
Wire Wire Line
	6600 3400 6400 3400
$Comp
L SemiparamEQ1-rescue:LM4565FVM-GTR-Codesign U5
U 1 1 5DA5094F
P 5200 5750
F 0 "U5" H 5200 6117 50  0000 C CNN
F 1 "LM4565FVM-GTR" H 5200 6026 50  0000 C CNN
F 2 "Codesign:LM4565FVM-GTR-MSOP-8" H 5200 5750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm358.pdf" H 5200 5750 50  0001 C CNN
F 4 "x" H 5200 5750 50  0001 C CNN "onderdelen 5mar20"
	1    5200 5750
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM4565FVM-GTR-Codesign U5
U 2 1 5DA529A0
P 2900 5850
F 0 "U5" H 2900 5483 50  0000 C CNN
F 1 "LM4565FVM-GTR" H 2900 5574 50  0000 C CNN
F 2 "Codesign:LM4565FVM-GTR-MSOP-8" H 2900 5850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm358.pdf" H 2900 5850 50  0001 C CNN
F 4 "x" H 2900 5850 50  0001 C CNN "onderdelen 5mar20"
	2    2900 5850
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LM4565FVM-GTR-Codesign U5
U 3 1 5DA58453
P 1850 2500
F 0 "U5" H 1808 2546 50  0000 L CNN
F 1 "LM4565FVM-GTR" H 1808 2455 50  0000 L CNN
F 2 "Codesign:LM4565FVM-GTR-MSOP-8" H 1850 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm358.pdf" H 1850 2500 50  0001 C CNN
F 4 "x" H 1850 2500 50  0001 C CNN "onderdelen 5mar20"
	3    1850 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 2850 1750 2800
Connection ~ 1750 2800
Wire Wire Line
	1750 2150 1750 2200
Connection ~ 1750 2200
Wire Wire Line
	5500 5750 5600 5750
Connection ~ 5600 5750
Wire Wire Line
	5600 5750 5750 5750
Wire Wire Line
	3200 5650 3250 5650
Text Notes 4100 6200 0    50   ~ 0
Subtract 1V from V_ampl in conf A
Wire Wire Line
	5600 4750 5600 5750
Wire Wire Line
	2350 5750 2550 5750
Wire Wire Line
	2550 5300 2550 5750
Connection ~ 2550 5750
Wire Wire Line
	2550 5750 2600 5750
Wire Wire Line
	2750 5300 2550 5300
$Comp
L SemiparamEQ1-rescue:R_POT_TRIM-Device RV?
U 1 1 5DD57B1D
P 3950 5200
AR Path="/5DC0D667/5DD57B1D" Ref="RV?"  Part="1" 
AR Path="/5DB4A226/5DD57B1D" Ref="RV1"  Part="1" 
F 0 "RV1" H 3880 5154 50  0000 R CNN
F 1 "Trim_Vampl" H 3880 5245 50  0000 R CNN
F 2 "Codesign:Potentiometer_Bourns_3214W_Vertical_HandSolder" H 3950 5200 50  0001 C CNN
F 3 "~" H 3950 5200 50  0001 C CNN
F 4 "x" H 3950 5200 50  0001 C CNN "onderdelen 5mar20"
	1    3950 5200
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:1N4148-Diode D1
U 1 1 5DD5C35C
P 4200 4950
F 0 "D1" V 4154 5029 50  0000 L CNN
F 1 "1N4148" V 4245 5029 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4200 4775 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4200 4950 50  0001 C CNN
F 4 "x" H 4200 4950 50  0001 C CNN "onderdelen 5mar20"
	1    4200 4950
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DD5CC71
P 3950 5350
AR Path="/5DD5CC71" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DD5CC71" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 3950 5100 50  0001 C CNN
F 1 "GND" H 3955 5177 50  0000 C CNN
F 2 "" H 3950 5350 50  0001 C CNN
F 3 "" H 3950 5350 50  0001 C CNN
	1    3950 5350
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR026
U 1 1 5DD5D260
P 3950 5050
F 0 "#PWR026" H 3950 4900 50  0001 C CNN
F 1 "+5V" H 3965 5223 50  0000 C CNN
F 2 "" H 3950 5050 50  0001 C CNN
F 3 "" H 3950 5050 50  0001 C CNN
	1    3950 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5200 4200 5200
Wire Wire Line
	4200 5200 4200 5100
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP3
U 1 1 5DD68652
P 3500 4600
F 0 "TP3" H 3558 4718 50  0000 L CNN
F 1 "PotVampl" H 3558 4627 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 3700 4600 50  0001 C CNN
F 3 "~" H 3700 4600 50  0001 C CNN
F 4 "x" H 3500 4600 50  0001 C CNN "onderdelen 5mar20"
	1    3500 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4600 3500 4750
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DBC64EF
P 1600 5950
AR Path="/5DBC64EF" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DBC64EF" Ref="R19"  Part="1" 
F 0 "R19" H 1670 5996 50  0000 L CNN
F 1 "10k" H 1670 5905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1530 5950 50  0001 C CNN
F 3 "~" H 1600 5950 50  0001 C CNN
F 4 "x" H 1600 5950 50  0001 C CNN "onderdelen 5mar20"
	1    1600 5950
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:MCP6L02T-E_SN-Codesign U14
U 2 1 5DB56E72
P 4750 4100
F 0 "U14" H 4800 4300 50  0000 C CNN
F 1 "LMV358AIDR" H 4800 4400 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4750 4100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1669460.pdf" H 4750 4100 50  0001 C CNN
F 4 "x" H 4750 4100 50  0001 C CNN "onderdelen 5mar20"
	2    4750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 5750 2050 5750
Wire Wire Line
	4800 4750 4800 4950
Connection ~ 4800 5650
Wire Wire Line
	4800 5650 4750 5650
Wire Wire Line
	4200 4800 4200 4750
Connection ~ 4200 4750
Wire Wire Line
	4450 4200 4450 4350
Wire Wire Line
	4450 4350 5100 4350
Wire Wire Line
	5100 4350 5100 4100
Wire Wire Line
	5100 4100 5050 4100
Connection ~ 5100 4100
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DEFD5E0
P 3800 4000
AR Path="/5DEFD5E0" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DEFD5E0" Ref="R25"  Part="1" 
F 0 "R25" V 3593 4000 50  0000 C CNN
F 1 "100k" V 3684 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3730 4000 50  0001 C CNN
F 3 "~" H 3800 4000 50  0001 C CNN
F 4 "x" H 3800 4000 50  0001 C CNN "onderdelen 5mar20"
	1    3800 4000
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DEFDB17
P 4050 4250
AR Path="/5DEFDB17" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5DEFDB17" Ref="R26"  Part="1" 
F 0 "R26" H 3900 4200 50  0000 C CNN
F 1 "100k" H 3900 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3980 4250 50  0001 C CNN
F 3 "~" H 4050 4250 50  0001 C CNN
F 4 "x" H 4050 4250 50  0001 C CNN "onderdelen 5mar20"
	1    4050 4250
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DEFE00C
P 4050 4400
AR Path="/5DEFE00C" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DEFE00C" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 4050 4150 50  0001 C CNN
F 1 "GND" H 4055 4227 50  0000 C CNN
F 2 "" H 4050 4400 50  0001 C CNN
F 3 "" H 4050 4400 50  0001 C CNN
	1    4050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4000 4050 4000
Wire Wire Line
	4050 4100 4050 4000
Connection ~ 4050 4000
Wire Wire Line
	4050 4000 4450 4000
Text Notes 5150 4700 0    50   ~ 0
Conf A 1V-4V
Text Notes 5150 4250 0    50   ~ 0
Conf B 0V-6V\n
$Comp
L SemiparamEQ1-rescue:Conn_01x01_Female-Connector J?
U 1 1 5DF0F197
P 3100 3550
AR Path="/5DCEAD28/5DF0F197" Ref="J?"  Part="1" 
AR Path="/5DB4A226/5DF0F197" Ref="J2"  Part="1" 
F 0 "J2" V 3038 3462 50  0000 R CNN
F 1 "AMPL" V 2947 3462 50  0000 R CNN
F 2 "Connector:Banana_Jack_1Pin" H 3100 3550 50  0001 C CNN
F 3 "~" H 3100 3550 50  0001 C CNN
F 4 "x" H 3100 3550 50  0001 C CNN "onderdelen 5mar20"
	1    3100 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4800 5250 4800 5650
Wire Wire Line
	4200 4750 4800 4750
Wire Wire Line
	3250 5650 3250 5300
Wire Wire Line
	3250 5300 3050 5300
Wire Wire Line
	4450 5650 3250 5650
Connection ~ 3250 5650
Text HLabel 1000 4400 0    50   Input ~ 0
V_ampl
$Comp
L SemiparamEQ1-rescue:MCP6L02T-E_SN-Codesign U14
U 3 1 5DF74BF0
P 3200 2650
AR Path="/5DB4A226/5DF74BF0" Ref="U14"  Part="3" 
AR Path="/5DCEAD28/5DF74BF0" Ref="U?"  Part="3" 
F 0 "U14" H 3158 2696 50  0000 L CNN
F 1 "LMV358AIDR" H 3158 2605 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3200 2650 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1669460.pdf" H 3200 2650 50  0001 C CNN
F 4 "x" H 3200 2650 50  0001 C CNN "onderdelen 5mar20"
	3    3200 2650
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C14
U 1 1 5DF74BF6
P 2800 2650
AR Path="/5DB4A226/5DF74BF6" Ref="C14"  Part="1" 
AR Path="/5DCEAD28/5DF74BF6" Ref="C?"  Part="1" 
F 0 "C14" V 2548 2650 50  0000 C CNN
F 1 "100n" V 2639 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2838 2500 50  0001 C CNN
F 3 "~" H 2800 2650 50  0001 C CNN
F 4 "x" H 2800 2650 50  0001 C CNN "onderdelen 5mar20"
	1    2800 2650
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DF74BFC
P 3100 2950
AR Path="/5DF74BFC" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DF74BFC" Ref="#PWR025"  Part="1" 
AR Path="/5DCEAD28/5DF74BFC" Ref="#PWR?"  Part="1" 
F 0 "#PWR025" H 3100 2700 50  0001 C CNN
F 1 "GND" H 3105 2777 50  0000 C CNN
F 2 "" H 3100 2950 50  0001 C CNN
F 3 "" H 3100 2950 50  0001 C CNN
	1    3100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2950 2800 2950
Wire Wire Line
	2800 2950 2800 2800
Connection ~ 3100 2950
Wire Wire Line
	2800 2500 2800 2350
Wire Wire Line
	2800 2350 3100 2350
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR024
U 1 1 5DF74C07
P 3100 2350
AR Path="/5DB4A226/5DF74C07" Ref="#PWR024"  Part="1" 
AR Path="/5DCEAD28/5DF74C07" Ref="#PWR?"  Part="1" 
F 0 "#PWR024" H 3100 2200 50  0001 C CNN
F 1 "+5V" H 3115 2523 50  0000 C CNN
F 2 "" H 3100 2350 50  0001 C CNN
F 3 "" H 3100 2350 50  0001 C CNN
	1    3100 2350
	1    0    0    -1  
$EndComp
Connection ~ 3100 2350
$Comp
L Jumper:Jumper_3_Open JP1
U 1 1 5E322387
P 5700 4450
F 0 "JP1" V 5654 4537 50  0000 L CNN
F 1 "Jumper_3_Open" V 5750 4550 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5700 4450 50  0001 C CNN
F 3 "~" H 5700 4450 50  0001 C CNN
F 4 "x" H 5700 4450 50  0001 C CNN "onderdelen 5mar20"
	1    5700 4450
	0    -1   1    0   
$EndComp
Wire Wire Line
	5900 4750 6250 4750
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5E4B27EB
P 1450 4400
AR Path="/5E4B27EB" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5E4B27EB" Ref="R23"  Part="1" 
F 0 "R23" V 1243 4400 50  0000 C CNN
F 1 "10k" V 1334 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1380 4400 50  0001 C CNN
F 3 "~" H 1450 4400 50  0001 C CNN
F 4 "x" H 1450 4400 50  0001 C CNN "onderdelen 5mar20"
	1    1450 4400
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5E4B2D4B
P 1150 4650
AR Path="/5E4B2D4B" Ref="R?"  Part="1" 
AR Path="/5DB4A226/5E4B2D4B" Ref="R22"  Part="1" 
F 0 "R22" H 1000 4600 50  0000 C CNN
F 1 "22k" H 1000 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1080 4650 50  0001 C CNN
F 3 "~" H 1150 4650 50  0001 C CNN
F 4 "x" H 1150 4650 50  0001 C CNN "onderdelen 5mar20"
	1    1150 4650
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E4B3165
P 1150 4800
AR Path="/5E4B3165" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5E4B3165" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 1150 4550 50  0001 C CNN
F 1 "GND" H 1155 4627 50  0000 C CNN
F 2 "" H 1150 4800 50  0001 C CNN
F 3 "" H 1150 4800 50  0001 C CNN
	1    1150 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 4400 1150 4400
Wire Wire Line
	1150 4500 1150 4400
Connection ~ 1150 4400
Wire Wire Line
	1150 4400 1300 4400
Wire Wire Line
	1600 4400 1700 4400
Wire Wire Line
	5900 3300 5900 4450
Wire Wire Line
	5600 4750 5700 4750
Wire Wire Line
	5700 4750 5700 4700
Wire Wire Line
	5850 4450 5900 4450
Connection ~ 5900 4450
Wire Wire Line
	5900 4450 5900 4750
Wire Wire Line
	3400 4750 3500 4750
Wire Wire Line
	5700 4100 5700 4200
Wire Wire Line
	5100 4100 5700 4100
Connection ~ 3500 4750
Wire Wire Line
	3500 4750 4200 4750
$Comp
L Jumper:Jumper_3_Open JP3
U 1 1 5E4B2DBB
P 3100 4400
F 0 "JP3" V 2900 4150 50  0000 L CNN
F 1 "Jumper_3_Open" V 3000 3750 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3100 4400 50  0001 C CNN
F 3 "~" H 3100 4400 50  0001 C CNN
F 4 "x" H 3100 4400 50  0001 C CNN "onderdelen 5mar20"
	1    3100 4400
	0    -1   1    0   
$EndComp
Wire Wire Line
	3250 4400 3400 4400
Wire Wire Line
	3400 4400 3400 4750
Wire Wire Line
	3100 4000 3100 4150
Wire Wire Line
	3100 4000 3650 4000
Wire Wire Line
	3100 4000 3100 3750
Connection ~ 3100 4000
Wire Wire Line
	1700 4400 1700 4650
Wire Wire Line
	1700 4650 3100 4650
$Comp
L power:+12V #PWR0110
U 1 1 5E67A844
P 8700 2200
F 0 "#PWR0110" H 8700 2050 50  0001 C CNN
F 1 "+12V" H 8715 2373 50  0000 C CNN
F 2 "" H 8700 2200 50  0001 C CNN
F 3 "" H 8700 2200 50  0001 C CNN
	1    8700 2200
	1    0    0    -1  
$EndComp
Connection ~ 8700 2200
$Comp
L power:-12V #PWR0111
U 1 1 5E67AEB2
P 8700 2800
F 0 "#PWR0111" H 8700 2900 50  0001 C CNN
F 1 "-12V" H 8715 2973 50  0000 C CNN
F 2 "" H 8700 2800 50  0001 C CNN
F 3 "" H 8700 2800 50  0001 C CNN
	1    8700 2800
	-1   0    0    1   
$EndComp
Connection ~ 8700 2800
$Comp
L power:-12V #PWR0112
U 1 1 5E67BA90
P 7750 3250
F 0 "#PWR0112" H 7750 3350 50  0001 C CNN
F 1 "-12V" H 7765 3423 50  0000 C CNN
F 2 "" H 7750 3250 50  0001 C CNN
F 3 "" H 7750 3250 50  0001 C CNN
	1    7750 3250
	-1   0    0    1   
$EndComp
$Comp
L power:-12V #PWR0113
U 1 1 5E67BE62
P 4200 3200
F 0 "#PWR0113" H 4200 3300 50  0001 C CNN
F 1 "-12V" H 4215 3373 50  0000 C CNN
F 2 "" H 4200 3200 50  0001 C CNN
F 3 "" H 4200 3200 50  0001 C CNN
	1    4200 3200
	-1   0    0    1   
$EndComp
$Comp
L power:-12V #PWR0114
U 1 1 5E67D0A0
P 1750 2850
F 0 "#PWR0114" H 1750 2950 50  0001 C CNN
F 1 "-12V" H 1765 3023 50  0000 C CNN
F 2 "" H 1750 2850 50  0001 C CNN
F 3 "" H 1750 2850 50  0001 C CNN
	1    1750 2850
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0115
U 1 1 5E67D8AC
P 1750 2150
F 0 "#PWR0115" H 1750 2000 50  0001 C CNN
F 1 "+12V" H 1765 2323 50  0000 C CNN
F 2 "" H 1750 2150 50  0001 C CNN
F 3 "" H 1750 2150 50  0001 C CNN
	1    1750 2150
	1    0    0    -1  
$EndComp
Text HLabel 6400 3600 2    50   Output ~ 0
Rx_b
Wire Wire Line
	6400 3400 6400 3600
Text Notes 2650 4500 0    50   ~ 0
Vabc\nPotVampl\nXmega
Text Notes 5700 2900 0    50   ~ 0
Rabc 2880
$EndSCHEMATC
