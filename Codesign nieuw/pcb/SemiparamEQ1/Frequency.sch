EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7400 2500 7350 2500
Wire Wire Line
	7350 2800 7400 2800
Connection ~ 7400 2800
Wire Wire Line
	7400 2800 7400 2500
Wire Wire Line
	7350 2300 7550 2300
$Comp
L SemiparamEQ1-rescue:DS1267_TSSOP-Potentiometer_Digital U?
U 1 1 5DC106DD
P 6950 2600
AR Path="/5DC106DD" Ref="U?"  Part="1" 
AR Path="/5DC0D667/5DC106DD" Ref="U10"  Part="1" 
F 0 "U10" H 6950 3281 50  0000 C CNN
F 1 "DS1267_TSSOP" H 6950 3190 50  0000 C CNN
F 2 "Codesign:Digipot_TSSOP-20_4.4x6.5mm_P0.65mm" H 8050 2100 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/DS1267.pdf" H 6950 2650 50  0001 C CNN
F 4 "x" H 6950 2600 50  0001 C CNN "onderdelen 5mar20"
	1    6950 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1800 6950 1850
Wire Wire Line
	6950 3100 6950 3200
Wire Wire Line
	6950 3200 7400 3200
Wire Wire Line
	7400 2800 7400 3200
Connection ~ 7400 3200
$Comp
L SemiparamEQ1-rescue:C-Device C18
U 1 1 5DD49E0E
P 7250 1850
F 0 "C18" V 6998 1850 50  0000 C CNN
F 1 "100n" V 7089 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7288 1700 50  0001 C CNN
F 3 "~" H 7250 1850 50  0001 C CNN
F 4 "x" H 7250 1850 50  0001 C CNN "onderdelen 5mar20"
	1    7250 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 1850 6950 1850
Connection ~ 6950 1850
Wire Wire Line
	6950 1850 6950 2100
Wire Wire Line
	7400 1850 7400 2500
Connection ~ 7400 2500
$Comp
L SemiparamEQ1-rescue:C-Device C17
U 1 1 5DD4B4D6
P 4200 1950
F 0 "C17" H 4350 1950 50  0000 C CNN
F 1 "100n" H 4350 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4238 1800 50  0001 C CNN
F 3 "~" H 4200 1950 50  0001 C CNN
F 4 "x" H 4200 1950 50  0001 C CNN "onderdelen 5mar20"
	1    4200 1950
	-1   0    0    -1  
$EndComp
Text HLabel 7950 2300 2    50   Output ~ 0
Digipot_H0
Text HLabel 7950 2400 2    50   Output ~ 0
Digipot_W0
Text HLabel 7950 2600 2    50   Output ~ 0
Digipot_H1
Text HLabel 7950 2700 2    50   Output ~ 0
Digipot_W1
Wire Wire Line
	5650 2600 6200 2600
Wire Wire Line
	6200 2600 6200 2300
Wire Wire Line
	6550 2800 6200 2800
Wire Wire Line
	6200 2800 6200 2700
Wire Wire Line
	6200 2700 5650 2700
Text Notes 7550 3650 0    50   ~ 0
Setting VB of digipot to -5.5V means \nvoltage range on H0 and H1can be -5.5V to 5.5V\n\nWhen only 5V is active: should be set to GND
$Comp
L SemiparamEQ1-rescue:R-Device R42
U 1 1 5DD5DA0B
P 5800 1400
F 0 "R42" V 5593 1400 50  0000 C CNN
F 1 "0R_DP_H0" V 5684 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5730 1400 50  0001 C CNN
F 3 "~" H 5800 1400 50  0001 C CNN
F 4 "x" H 5800 1400 50  0001 C CNN "onderdelen 5mar20"
	1    5800 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 2300 7550 1400
Wire Wire Line
	7550 1400 5950 1400
Connection ~ 7550 2300
$Comp
L SemiparamEQ1-rescue:R_POT_TRIM-Device RV2
U 1 1 5DE66B26
P 2900 1900
F 0 "RV2" H 2830 1854 50  0000 R CNN
F 1 "Trim_Vfreq" H 2830 1945 50  0000 R CNN
F 2 "Codesign:Potentiometer_Bourns_3214W_Vertical_HandSolder" H 2900 1900 50  0001 C CNN
F 3 "~" H 2900 1900 50  0001 C CNN
F 4 "x" H 2900 1900 50  0001 C CNN "onderdelen 5mar20"
	1    2900 1900
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DE6AAD8
P 2900 2050
AR Path="/5DE6AAD8" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5DE6AAD8" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 2900 1800 50  0001 C CNN
F 1 "GND" H 2905 1877 50  0000 C CNN
F 2 "" H 2900 2050 50  0001 C CNN
F 3 "" H 2900 2050 50  0001 C CNN
	1    2900 2050
	1    0    0    -1  
$EndComp
Text Notes 5800 1100 0    50   ~ 0
Place this 0R to test digipot, in absence of regular H0 reference\n(set trimpot to max and measure TP_Digipot)
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DC106EF
P 7400 3650
AR Path="/5DC106EF" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5DC106EF" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 7400 3400 50  0001 C CNN
F 1 "GND" H 7405 3477 50  0000 C CNN
F 2 "" H 7400 3650 50  0001 C CNN
F 3 "" H 7400 3650 50  0001 C CNN
	1    7400 3650
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR?
U 1 1 5DB854F9
P 6950 1800
AR Path="/5DB854F9" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5DB854F9" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 6950 1650 50  0001 C CNN
F 1 "+5V" H 6965 1973 50  0000 C CNN
F 2 "" H 6950 1800 50  0001 C CNN
F 3 "" H 6950 1800 50  0001 C CNN
	1    6950 1800
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP8
U 1 1 5DEC7DEE
P 7800 2100
F 0 "TP8" H 7742 2126 50  0000 R CNN
F 1 "DP_W0" H 7742 2217 50  0000 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 8000 2100 50  0001 C CNN
F 3 "~" H 8000 2100 50  0001 C CNN
F 4 "x" H 7800 2100 50  0001 C CNN "onderdelen 5mar20"
	1    7800 2100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7550 2300 7950 2300
Wire Wire Line
	7350 2400 7800 2400
Wire Wire Line
	7350 2600 7950 2600
Wire Wire Line
	7350 2700 7950 2700
Wire Wire Line
	7800 2100 7800 2400
Connection ~ 7800 2400
Wire Wire Line
	7800 2400 7950 2400
Wire Wire Line
	6550 2300 6200 2300
Wire Wire Line
	6550 2400 5650 2400
Wire Wire Line
	6550 2500 5650 2500
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP7
U 1 1 5DD78185
P 4100 1200
F 0 "TP7" H 4158 1318 50  0000 L CNN
F 1 "PotVfreq" H 4158 1227 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 4300 1200 50  0001 C CNN
F 3 "~" H 4300 1200 50  0001 C CNN
F 4 "x" H 4100 1200 50  0001 C CNN "onderdelen 5mar20"
	1    4100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1200 4100 1400
Connection ~ 4100 1400
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DF46DE6
P 3650 2700
AR Path="/5DCEAD28/5DF46DE6" Ref="R?"  Part="1" 
AR Path="/5DC0D667/5DF46DE6" Ref="R39"  Part="1" 
F 0 "R39" H 3720 2746 50  0000 L CNN
F 1 "22k" H 3720 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3580 2700 50  0001 C CNN
F 3 "~" H 3650 2700 50  0001 C CNN
F 4 "x" H 3650 2700 50  0001 C CNN "onderdelen 5mar20"
	1    3650 2700
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5DF46DEC
P 3650 2250
AR Path="/5DCEAD28/5DF46DEC" Ref="R?"  Part="1" 
AR Path="/5DC0D667/5DF46DEC" Ref="R38"  Part="1" 
F 0 "R38" H 3500 2200 50  0000 C CNN
F 1 "10k" H 3500 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3580 2250 50  0001 C CNN
F 3 "~" H 3650 2250 50  0001 C CNN
F 4 "x" H 3650 2250 50  0001 C CNN "onderdelen 5mar20"
	1    3650 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 2550 3650 2400
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DF46DF5
P 3650 2850
AR Path="/5DCEAD28/5DF46DF5" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5DF46DF5" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 3650 2600 50  0001 C CNN
F 1 "GND" H 3655 2677 50  0000 C CNN
F 2 "" H 3650 2850 50  0001 C CNN
F 3 "" H 3650 2850 50  0001 C CNN
	1    3650 2850
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:Conn_01x01_Female-Connector J?
U 1 1 5DF46DFD
P 2950 1050
AR Path="/5DCEAD28/5DF46DFD" Ref="J?"  Part="1" 
AR Path="/5DC0D667/5DF46DFD" Ref="J3"  Part="1" 
F 0 "J3" V 2888 962 50  0000 R CNN
F 1 "FREQ" V 2797 962 50  0000 R CNN
F 2 "Connector:Banana_Jack_1Pin" H 2950 1050 50  0001 C CNN
F 3 "~" H 2950 1050 50  0001 C CNN
F 4 "x" H 2950 1050 50  0001 C CNN "onderdelen 5mar20"
	1    2950 1050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3650 2400 3900 2400
Connection ~ 3650 2400
Wire Wire Line
	4100 1400 5650 1400
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR?
U 1 1 5DF56E1A
P 2900 1750
AR Path="/5DF56E1A" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5DF56E1A" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 2900 1600 50  0001 C CNN
F 1 "+5V" H 2915 1923 50  0000 C CNN
F 2 "" H 2900 1750 50  0001 C CNN
F 3 "" H 2900 1750 50  0001 C CNN
	1    2900 1750
	1    0    0    -1  
$EndComp
Text HLabel 3150 3150 0    50   Input ~ 0
V_ampl
Connection ~ 3900 2400
Wire Wire Line
	3900 2400 4400 2400
Wire Wire Line
	3900 3250 3900 2400
Wire Wire Line
	3900 3250 3950 3250
Wire Wire Line
	3300 2500 4400 2500
Wire Wire Line
	3300 3350 3550 3350
Wire Wire Line
	3300 2500 3300 3150
Wire Wire Line
	3150 3150 3300 3150
Connection ~ 3300 3150
Wire Wire Line
	3300 3150 3300 3350
Wire Wire Line
	2950 1400 2950 1250
Wire Wire Line
	2950 1400 3650 1400
Wire Wire Line
	3650 2100 3650 1900
Connection ~ 3650 1400
Wire Wire Line
	3650 1400 4100 1400
Wire Wire Line
	3050 1900 3200 1900
Connection ~ 3650 1900
Wire Wire Line
	3650 1900 3650 1400
$Comp
L SemiparamEQ1-rescue:R-Device R41
U 1 1 5DFB74DC
P 4100 3250
F 0 "R41" V 3893 3250 50  0000 C CNN
F 1 "dnp0" V 3984 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4030 3250 50  0001 C CNN
F 3 "~" H 4100 3250 50  0001 C CNN
F 4 "x" H 4100 3250 50  0001 C CNN "onderdelen 5mar20"
	1    4100 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 3250 4400 3250
$Comp
L SemiparamEQ1-rescue:R-Device R40
U 1 1 5DFB7E3E
P 3700 3350
F 0 "R40" V 3493 3350 50  0000 C CNN
F 1 "dnp1" V 3584 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3630 3350 50  0001 C CNN
F 3 "~" H 3700 3350 50  0001 C CNN
F 4 "x" H 3700 3350 50  0001 C CNN "onderdelen 5mar20"
	1    3700 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 3350 4400 3350
Text Notes 5650 2400 0    50   ~ 0
SPI
Connection ~ 4400 4550
Wire Wire Line
	4400 4550 4400 4450
Connection ~ 4400 4450
Wire Wire Line
	4400 4450 4400 4350
Connection ~ 4400 4350
Wire Wire Line
	4400 4350 4400 4250
Wire Wire Line
	4400 4250 4400 4150
Connection ~ 4400 4250
$Comp
L SemiparamEQ1-rescue:Xmega-HVA-Xmega-HVA-header U?
U 1 1 5DC10702
P 5000 3150
AR Path="/5DC10702" Ref="U?"  Part="1" 
AR Path="/5DC0D667/5DC10702" Ref="U8"  Part="1" 
F 0 "U8" H 5025 4465 50  0000 C CNN
F 1 "Xmega-HVA" H 5025 4374 50  0000 C CNN
F 2 "Codesign:Xmega-HVA-Header" H 5000 3150 50  0001 C CNN
F 3 "" H 5000 3150 50  0001 C CNN
F 4 "x" H 5000 3150 50  0001 C CNN "onderdelen 5mar20"
	1    5000 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4550 4400 4600
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DD4AAB3
P 4400 4600
AR Path="/5DD4AAB3" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5DD4AAB3" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 4400 4350 50  0001 C CNN
F 1 "GND" H 4405 4427 50  0000 C CNN
F 2 "" H 4400 4600 50  0001 C CNN
F 3 "" H 4400 4600 50  0001 C CNN
	1    4400 4600
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E434CEE
P 4200 2150
AR Path="/5E434CEE" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5E434CEE" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 4200 1900 50  0001 C CNN
F 1 "GND" H 4205 1977 50  0000 C CNN
F 2 "" H 4200 2150 50  0001 C CNN
F 3 "" H 4200 2150 50  0001 C CNN
	1    4200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1700 4400 1800
Wire Wire Line
	4200 2100 4200 2150
Wire Wire Line
	4200 1800 4400 1800
Connection ~ 4400 1800
Wire Wire Line
	4400 1800 4400 2100
$Comp
L SemiparamEQ1-rescue:D-Device D10
U 1 1 5E4A816C
P 3350 1900
AR Path="/5E4A816C" Ref="D10"  Part="1" 
AR Path="/5DC0D667/5E4A816C" Ref="D10"  Part="1" 
F 0 "D10" H 3350 1684 50  0000 C CNN
F 1 "1N4148" H 3350 1775 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3350 1900 50  0001 C CNN
F 3 "~" H 3350 1900 50  0001 C CNN
F 4 "x" H 3350 1900 50  0001 C CNN "onderdelen 5mar20"
	1    3350 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 1900 3650 1900
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5E584A09
P 6800 3500
AR Path="/5DCEAD28/5E584A09" Ref="R?"  Part="1" 
AR Path="/5DC0D667/5E584A09" Ref="R43"  Part="1" 
F 0 "R43" V 6700 3550 50  0000 C CNN
F 1 "270k" V 6600 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6730 3500 50  0001 C CNN
F 3 "~" H 6800 3500 50  0001 C CNN
F 4 "x" H 6800 3500 50  0001 C CNN "onderdelen 5mar20"
	1    6800 3500
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5E585740
P 7250 3500
AR Path="/5DCEAD28/5E585740" Ref="R?"  Part="1" 
AR Path="/5DC0D667/5E585740" Ref="R44"  Part="1" 
F 0 "R44" V 7150 3550 50  0000 C CNN
F 1 "220k" V 7050 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7180 3500 50  0001 C CNN
F 3 "~" H 7250 3500 50  0001 C CNN
F 4 "x" H 7250 3500 50  0001 C CNN "onderdelen 5mar20"
	1    7250 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 3650 7400 3500
Connection ~ 7400 3500
Wire Wire Line
	7400 3500 7400 3200
Wire Wire Line
	7100 3500 7050 3500
Wire Wire Line
	7050 3100 7050 3500
Connection ~ 7050 3500
Wire Wire Line
	7050 3500 6950 3500
Wire Wire Line
	6650 3500 6500 3500
Wire Wire Line
	6500 3500 6500 3650
Text HLabel 4400 1700 2    50   Input ~ 0
VinXmega
Text HLabel 5650 1700 0    50   Input ~ 0
VRAW
Wire Wire Line
	5650 1700 5650 2100
$Comp
L Connector:Conn_01x04_Female J10
U 1 1 5E5F7726
P 6550 4200
F 0 "J10" H 6578 4176 50  0000 L CNN
F 1 "UART" H 6578 4085 50  0000 L CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MC_1,5_4-G-3.81_1x04_P3.81mm_Horizontal" H 6550 4200 50  0001 C CNN
F 3 "~" H 6550 4200 50  0001 C CNN
F 4 "x" H 6550 4200 50  0001 C CNN "onderdelen 5mar20"
	1    6550 4200
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E5FC22D
P 6300 4500
AR Path="/5E5FC22D" Ref="#PWR?"  Part="1" 
AR Path="/5DC0D667/5E5FC22D" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 6300 4250 50  0001 C CNN
F 1 "GND" H 6305 4327 50  0000 C CNN
F 2 "" H 6300 4500 50  0001 C CNN
F 3 "" H 6300 4500 50  0001 C CNN
	1    6300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 4500 6300 4400
Wire Wire Line
	6300 4400 6350 4400
Wire Wire Line
	5650 3950 5750 3950
Wire Wire Line
	5750 3950 5750 4200
Wire Wire Line
	5750 4200 6350 4200
Wire Wire Line
	6350 4300 5900 4300
Wire Wire Line
	5900 4300 5900 3850
Wire Wire Line
	5900 3850 5650 3850
Text Notes 6800 4400 0    50   ~ 0
3v3\nTx\nRx\nGND
Text Notes 5500 4300 0    50   ~ 0
UsartE1
Wire Wire Line
	5650 2200 6000 2200
Wire Wire Line
	6000 2200 6000 4100
Wire Wire Line
	6000 4100 6350 4100
$Comp
L power:-12V #PWR0116
U 1 1 5E67FA65
P 6500 3650
F 0 "#PWR0116" H 6500 3750 50  0001 C CNN
F 1 "-12V" H 6515 3823 50  0000 C CNN
F 2 "" H 6500 3650 50  0001 C CNN
F 3 "" H 6500 3650 50  0001 C CNN
	1    6500 3650
	-1   0    0    1   
$EndComp
$EndSCHEMATC
