EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5800 5450 0    50   Input ~ 0
V_out
$Comp
L SemiparamEQ1-rescue:TestPoint_Probe-Connector TP13
U 1 1 5DD7195D
P 6600 5050
F 0 "TP13" H 6753 5151 50  0000 L CNN
F 1 "VoutAC" H 6650 5050 50  0000 L CNN
F 2 "Codesign:TestPoint_Pad_3.4x4.7mm" H 6800 5050 50  0001 C CNN
F 3 "~" H 6800 5050 50  0001 C CNN
F 4 "x" H 6600 5050 50  0001 C CNN "onderdelen 5mar20"
	1    6600 5050
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR056
U 1 1 5DD794BB
P 7000 6050
F 0 "#PWR056" H 7000 5800 50  0001 C CNN
F 1 "GND" H 7005 5877 50  0000 C CNN
F 2 "" H 7000 6050 50  0001 C CNN
F 3 "" H 7000 6050 50  0001 C CNN
	1    7000 6050
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:CP1-Device C26
U 1 1 5DD79CA4
P 6200 5450
F 0 "C26" V 6452 5450 50  0000 C CNN
F 1 "470u" V 6361 5450 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 6200 5450 50  0001 C CNN
F 3 "~" H 6200 5450 50  0001 C CNN
F 4 "x" H 6200 5450 50  0001 C CNN "onderdelen 5mar20"
	1    6200 5450
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R46
U 1 1 5DD7B188
P 6850 5800
F 0 "R46" H 6920 5846 50  0000 L CNN
F 1 "1k" H 6920 5755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6780 5800 50  0001 C CNN
F 3 "~" H 6850 5800 50  0001 C CNN
F 4 "x" H 6850 5800 50  0001 C CNN "onderdelen 5mar20"
	1    6850 5800
	1    0    0    -1  
$EndComp
Text HLabel 9000 1150 2    50   Output ~ 0
V_in
$Comp
L SemiparamEQ1-rescue:Conn_01x02-Connector_Generic J8
U 1 1 5DD7C603
P 5850 900
F 0 "J8" H 5768 1117 50  0000 C CNN
F 1 "GND" H 5768 1026 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 5850 900 50  0001 C CNN
F 3 "~" H 5850 900 50  0001 C CNN
F 4 "x" H 5850 900 50  0001 C CNN "onderdelen 5mar20"
	1    5850 900 
	-1   0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR058
U 1 1 5DD7CBA5
P 6050 1200
F 0 "#PWR058" H 6050 950 50  0001 C CNN
F 1 "GND" H 6055 1027 50  0000 C CNN
F 2 "" H 6050 1200 50  0001 C CNN
F 3 "" H 6050 1200 50  0001 C CNN
	1    6050 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1200 6050 1000
Wire Wire Line
	6050 900  6050 1000
Connection ~ 6050 1000
$Comp
L SemiparamEQ1-rescue:C-Device C27
U 1 1 5DD9706D
P 8450 1450
F 0 "C27" H 8650 1500 50  0000 C CNN
F 1 "1u" H 8650 1400 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W6.0mm_P5.00mm" H 8488 1300 50  0001 C CNN
F 3 "~" H 8450 1450 50  0001 C CNN
F 4 "x" H 8450 1450 50  0001 C CNN "onderdelen 5mar20"
	1    8450 1450
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:Q_NPN_BCE-Device Q2
U 1 1 5DD9DF12
P 10550 4150
F 0 "Q2" H 10741 4196 50  0000 L CNN
F 1 "Q_NPN_BCE" H 10741 4105 50  0000 L CNN
F 2 "Codesign:SOT-23_HandSolder" H 10750 4250 50  0001 C CNN
F 3 "~" H 10550 4150 50  0001 C CNN
F 4 "x" H 10550 4150 50  0001 C CNN "onderdelen 5mar20"
	1    10550 4150
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LED-Device D9
U 1 1 5DD9F502
P 10650 3650
F 0 "D9" V 10689 3533 50  0000 R CNN
F 1 "INPUT" V 10598 3533 50  0000 R CNN
F 2 "LED_SMD:LED_PLCC_2835_Handsoldering" H 10650 3650 50  0001 C CNN
F 3 "~" H 10650 3650 50  0001 C CNN
F 4 "x" H 10650 3650 50  0001 C CNN "onderdelen 5mar20"
	1    10650 3650
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR064
U 1 1 5DDA0A6C
P 9750 4500
F 0 "#PWR064" H 9750 4250 50  0001 C CNN
F 1 "GND" H 9755 4327 50  0000 C CNN
F 2 "" H 9750 4500 50  0001 C CNN
F 3 "" H 9750 4500 50  0001 C CNN
	1    9750 4500
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR072
U 1 1 5DDA0D1B
P 10650 4400
F 0 "#PWR072" H 10650 4150 50  0001 C CNN
F 1 "GND" H 10655 4227 50  0000 C CNN
F 2 "" H 10650 4400 50  0001 C CNN
F 3 "" H 10650 4400 50  0001 C CNN
	1    10650 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 3800 10650 3950
Wire Wire Line
	9750 4450 9750 4500
Wire Wire Line
	9450 4150 9300 4150
Wire Wire Line
	8850 3850 8850 4150
$Comp
L SemiparamEQ1-rescue:MIC5205-5.0YM5-Regulator_Linear U12
U 1 1 5DDF8D64
P 3050 2350
F 0 "U12" H 3050 2692 50  0000 C CNN
F 1 "MIC5205-5.0YM5" H 3050 2601 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 3050 2675 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005785A.pdf" H 3050 2350 50  0001 C CNN
F 4 "x" H 3050 2350 50  0001 C CNN "onderdelen 5mar20"
	1    3050 2350
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:Ferrite_Bead-Device FB1
U 1 1 5DDFCB39
P 2100 1500
F 0 "FB1" V 1826 1500 50  0000 C CNN
F 1 "Ferrite_Bead" V 1917 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2030 1500 50  0001 C CNN
F 3 "~" H 2100 1500 50  0001 C CNN
F 4 "x" H 2100 1500 50  0001 C CNN "onderdelen 5mar20"
	1    2100 1500
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C21
U 1 1 5DDFD13C
P 3400 2500
F 0 "C21" H 3515 2546 50  0000 L CNN
F 1 "470p" H 3515 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3438 2350 50  0001 C CNN
F 3 "~" H 3400 2500 50  0001 C CNN
F 4 "x" H 3400 2500 50  0001 C CNN "onderdelen 5mar20"
	1    3400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2250 2650 2350
Wire Wire Line
	2650 2350 2750 2350
Wire Wire Line
	2650 2250 2750 2250
$Comp
L SemiparamEQ1-rescue:GND-power #PWR051
U 1 1 5DDFFED2
P 2600 2650
F 0 "#PWR051" H 2600 2400 50  0001 C CNN
F 1 "GND" H 2605 2477 50  0000 C CNN
F 2 "" H 2600 2650 50  0001 C CNN
F 3 "" H 2600 2650 50  0001 C CNN
	1    2600 2650
	1    0    0    -1  
$EndComp
Connection ~ 3050 2650
Wire Wire Line
	3050 2650 2600 2650
Wire Wire Line
	2350 1800 2350 1900
Wire Wire Line
	3350 2350 3400 2350
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR053
U 1 1 5DE07327
P 4950 1600
F 0 "#PWR053" H 4950 1450 50  0001 C CNN
F 1 "+5V" H 4950 1750 50  0000 C CNN
F 2 "" H 4950 1600 50  0001 C CNN
F 3 "" H 4950 1600 50  0001 C CNN
	1    4950 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 2650 3400 2650
Connection ~ 3400 2650
Wire Wire Line
	4000 2350 4000 2250
Connection ~ 4000 2250
$Comp
L SemiparamEQ1-rescue:C-Device C30
U 1 1 5DE164BA
P 9750 4300
F 0 "C30" H 9865 4346 50  0000 L CNN
F 1 "100n" H 9865 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9788 4150 50  0001 C CNN
F 3 "~" H 9750 4300 50  0001 C CNN
F 4 "x" H 9750 4300 50  0001 C CNN "onderdelen 5mar20"
	1    9750 4300
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R51
U 1 1 5DE1765C
P 9150 4150
F 0 "R51" V 8943 4150 50  0000 C CNN
F 1 "50k" V 9034 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9080 4150 50  0001 C CNN
F 3 "~" H 9150 4150 50  0001 C CNN
F 4 "x" H 9150 4150 50  0001 C CNN "onderdelen 5mar20"
	1    9150 4150
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R49
U 1 1 5DE17FDA
P 8850 4400
F 0 "R49" H 8780 4354 50  0000 R CNN
F 1 "1k" H 8780 4445 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8780 4400 50  0001 C CNN
F 3 "~" H 8850 4400 50  0001 C CNN
F 4 "x" H 8850 4400 50  0001 C CNN "onderdelen 5mar20"
	1    8850 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 4150 8850 4150
$Comp
L SemiparamEQ1-rescue:GND-power #PWR060
U 1 1 5DE19704
P 8850 4650
F 0 "#PWR060" H 8850 4400 50  0001 C CNN
F 1 "GND" H 8855 4477 50  0000 C CNN
F 2 "" H 8850 4650 50  0001 C CNN
F 3 "" H 8850 4650 50  0001 C CNN
	1    8850 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 4650 8850 4550
Wire Wire Line
	8850 4250 8850 4150
Connection ~ 8850 4150
Wire Wire Line
	9450 2350 9300 2350
$Comp
L SemiparamEQ1-rescue:R-Device R50
U 1 1 5DE3D004
P 9150 2350
F 0 "R50" V 8943 2350 50  0000 C CNN
F 1 "40k" V 9034 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9080 2350 50  0001 C CNN
F 3 "~" H 9150 2350 50  0001 C CNN
F 4 "x" H 9150 2350 50  0001 C CNN "onderdelen 5mar20"
	1    9150 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	9000 2350 8850 2350
$Comp
L SemiparamEQ1-rescue:GND-power #PWR059
U 1 1 5DE3D011
P 8850 2850
F 0 "#PWR059" H 8850 2600 50  0001 C CNN
F 1 "GND" H 8855 2677 50  0000 C CNN
F 2 "" H 8850 2850 50  0001 C CNN
F 3 "" H 8850 2850 50  0001 C CNN
	1    8850 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 2850 8850 2750
$Comp
L SemiparamEQ1-rescue:R-Device R47
U 1 1 5DE50E95
P 8700 1150
F 0 "R47" V 8493 1150 50  0000 C CNN
F 1 "0R_Vin" V 8584 1150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8630 1150 50  0001 C CNN
F 3 "~" H 8700 1150 50  0001 C CNN
F 4 "x" H 8700 1150 50  0001 C CNN "onderdelen 5mar20"
	1    8700 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	8450 1150 8550 1150
Wire Wire Line
	8850 1150 9000 1150
$Comp
L SemiparamEQ1-rescue:R-Device R53
U 1 1 5DE5D748
P 10650 3350
F 0 "R53" H 10720 3396 50  0000 L CNN
F 1 "300" H 10720 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10580 3350 50  0001 C CNN
F 3 "~" H 10650 3350 50  0001 C CNN
F 4 "x" H 10650 3350 50  0001 C CNN "onderdelen 5mar20"
	1    10650 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 4350 10650 4400
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP10
U 1 1 5DE92EAC
P 3700 2250
F 0 "TP10" H 3750 2450 50  0000 L CNN
F 1 "+5V" H 3750 2350 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 3900 2250 50  0001 C CNN
F 3 "~" H 3900 2250 50  0001 C CNN
F 4 "x" H 3700 2250 50  0001 C CNN "onderdelen 5mar20"
	1    3700 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 2050 8850 2350
Connection ~ 8850 2350
Wire Wire Line
	8850 2350 8850 2450
Wire Wire Line
	9450 3750 9450 4150
$Comp
L SemiparamEQ1-rescue:GND-power #PWR068
U 1 1 5DEF3131
P 10150 4500
F 0 "#PWR068" H 10150 4250 50  0001 C CNN
F 1 "GND" H 10155 4327 50  0000 C CNN
F 2 "" H 10150 4500 50  0001 C CNN
F 3 "" H 10150 4500 50  0001 C CNN
	1    10150 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 4450 10150 4500
$Comp
L SemiparamEQ1-rescue:C-Device C32
U 1 1 5DEF3138
P 10150 4300
F 0 "C32" H 10265 4346 50  0000 L CNN
F 1 "100n" H 10265 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10188 4150 50  0001 C CNN
F 3 "~" H 10150 4300 50  0001 C CNN
F 4 "x" H 10150 4300 50  0001 C CNN "onderdelen 5mar20"
	1    10150 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 4150 10150 4150
Connection ~ 10150 4150
Wire Wire Line
	10150 4150 10350 4150
Wire Wire Line
	9450 1950 9450 2350
$Comp
L SemiparamEQ1-rescue:Q_NPN_BCE-Device Q1
U 1 1 5DF024E0
P 10550 2350
F 0 "Q1" H 10741 2396 50  0000 L CNN
F 1 "Q_NPN_BCE" H 10741 2305 50  0000 L CNN
F 2 "Codesign:SOT-23_HandSolder" H 10750 2450 50  0001 C CNN
F 3 "~" H 10550 2350 50  0001 C CNN
F 4 "x" H 10550 2350 50  0001 C CNN "onderdelen 5mar20"
	1    10550 2350
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LED-Device D8
U 1 1 5DF024EC
P 10650 1850
F 0 "D8" V 10689 1733 50  0000 R CNN
F 1 "CLIP" V 10598 1733 50  0000 R CNN
F 2 "LED_SMD:LED_PLCC_2835_Handsoldering" H 10650 1850 50  0001 C CNN
F 3 "~" H 10650 1850 50  0001 C CNN
F 4 "x" H 10650 1850 50  0001 C CNN "onderdelen 5mar20"
	1    10650 1850
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR063
U 1 1 5DF024F2
P 9750 2700
F 0 "#PWR063" H 9750 2450 50  0001 C CNN
F 1 "GND" H 9755 2527 50  0000 C CNN
F 2 "" H 9750 2700 50  0001 C CNN
F 3 "" H 9750 2700 50  0001 C CNN
	1    9750 2700
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR070
U 1 1 5DF024F8
P 10650 2600
F 0 "#PWR070" H 10650 2350 50  0001 C CNN
F 1 "GND" H 10655 2427 50  0000 C CNN
F 2 "" H 10650 2600 50  0001 C CNN
F 3 "" H 10650 2600 50  0001 C CNN
	1    10650 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 2000 10650 2150
Wire Wire Line
	9750 2650 9750 2700
$Comp
L SemiparamEQ1-rescue:C-Device C29
U 1 1 5DF02500
P 9750 2500
F 0 "C29" H 9865 2546 50  0000 L CNN
F 1 "100n" H 9865 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9788 2350 50  0001 C CNN
F 3 "~" H 9750 2500 50  0001 C CNN
F 4 "x" H 9750 2500 50  0001 C CNN "onderdelen 5mar20"
	1    9750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 2550 10650 2600
$Comp
L SemiparamEQ1-rescue:GND-power #PWR067
U 1 1 5DF02514
P 10150 2700
F 0 "#PWR067" H 10150 2450 50  0001 C CNN
F 1 "GND" H 10155 2527 50  0000 C CNN
F 2 "" H 10150 2700 50  0001 C CNN
F 3 "" H 10150 2700 50  0001 C CNN
	1    10150 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 2650 10150 2700
$Comp
L SemiparamEQ1-rescue:C-Device C31
U 1 1 5DF0251B
P 10150 2500
F 0 "C31" H 10265 2546 50  0000 L CNN
F 1 "100n" H 10265 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10188 2350 50  0001 C CNN
F 3 "~" H 10150 2500 50  0001 C CNN
F 4 "x" H 10150 2500 50  0001 C CNN "onderdelen 5mar20"
	1    10150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 2350 10150 2350
Connection ~ 10150 2350
Wire Wire Line
	10150 2350 10350 2350
$Comp
L SemiparamEQ1-rescue:GND-power #PWR047
U 1 1 5DDEC482
P 1700 2600
F 0 "#PWR047" H 1700 2350 50  0001 C CNN
F 1 "GND" H 1705 2427 50  0000 C CNN
F 2 "" H 1700 2600 50  0001 C CNN
F 3 "" H 1700 2600 50  0001 C CNN
	1    1700 2600
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR049
U 1 1 5DF2C3F3
P 2350 1900
F 0 "#PWR049" H 2350 1650 50  0001 C CNN
F 1 "GND" H 2355 1727 50  0000 C CNN
F 2 "" H 2350 1900 50  0001 C CNN
F 3 "" H 2350 1900 50  0001 C CNN
	1    2350 1900
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM2990SX-15-Regulator_Linear U13
U 1 1 5DF2F5D9
P 3350 3850
F 0 "U13" H 3350 4117 50  0000 C CNN
F 1 "LM2990SX-12" H 3350 4026 50  0000 C CNN
F 2 "Codesign:LM2940_KTT_TO-263-3_TabPin2" H 3350 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2990.pdf" H 2100 4600 50  0001 C CNN
F 4 "x" H 3350 3850 50  0001 C CNN "onderdelen 5mar20"
	1    3350 3850
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR050
U 1 1 5DEBEA41
P 2350 3350
F 0 "#PWR050" H 2350 3100 50  0001 C CNN
F 1 "GND" H 2355 3177 50  0000 C CNN
F 2 "" H 2350 3350 50  0001 C CNN
F 3 "" H 2350 3350 50  0001 C CNN
	1    2350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3300 3350 3550
$Comp
L SemiparamEQ1-rescue:CP1-Device C23
U 1 1 5DF5AFC8
P 4000 3550
F 0 "C23" H 4115 3596 50  0000 L CNN
F 1 "10u" H 4115 3505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 4000 3550 50  0001 C CNN
F 3 "~" H 4000 3550 50  0001 C CNN
F 4 "x" H 4000 3550 50  0001 C CNN "onderdelen 5mar20"
	1    4000 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3850 4000 3850
Wire Wire Line
	4000 3850 4000 3700
Wire Wire Line
	4000 3400 4000 3300
Wire Wire Line
	4000 3300 3350 3300
Connection ~ 3350 3300
Wire Wire Line
	4900 3700 4900 3850
Connection ~ 4000 3850
Wire Wire Line
	3000 1800 3000 1900
Wire Wire Line
	3000 1900 2350 1900
Connection ~ 2350 1900
Wire Wire Line
	2650 2250 2650 1500
Connection ~ 2650 2250
Connection ~ 2650 1500
Wire Wire Line
	2650 1500 2700 1500
$Comp
L SemiparamEQ1-rescue:CP1-Device C24
U 1 1 5DF821F2
P 4100 1700
F 0 "C24" H 4215 1746 50  0000 L CNN
F 1 "1u" H 4215 1655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 4100 1700 50  0001 C CNN
F 3 "~" H 4100 1700 50  0001 C CNN
F 4 "x" H 4100 1700 50  0001 C CNN "onderdelen 5mar20"
	1    4100 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1850 3550 1900
Wire Wire Line
	3550 1900 3000 1900
Connection ~ 3000 1900
Wire Wire Line
	4100 1400 3550 1400
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP9
U 1 1 5DF8F261
P 3550 1400
F 0 "TP9" H 3600 1600 50  0000 L CNN
F 1 "+12V" H 3600 1500 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 3750 1400 50  0001 C CNN
F 3 "~" H 3750 1400 50  0001 C CNN
F 4 "x" H 3550 1400 50  0001 C CNN "onderdelen 5mar20"
	1    3550 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP11
U 1 1 5DF93B13
P 4000 4150
F 0 "TP11" H 3942 4176 50  0000 R CNN
F 1 "-12V" H 3942 4267 50  0000 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 4200 4150 50  0001 C CNN
F 3 "~" H 4200 4150 50  0001 C CNN
F 4 "x" H 4000 4150 50  0001 C CNN "onderdelen 5mar20"
	1    4000 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 3850 4000 4150
Wire Wire Line
	8850 1850 8450 1850
Wire Wire Line
	8450 1650 8450 1850
Connection ~ 8450 1850
Wire Wire Line
	8850 3650 8450 3650
Text Notes 8000 700  0    50   ~ 10
SIGNAL INPUT
Text Notes 1350 950  0    50   ~ 10
POWER SUPPLIES
Text Notes 5650 4700 0    50   ~ 10
SIGNAL OUTPUT
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR069
U 1 1 5DFFB018
P 10650 1400
F 0 "#PWR069" H 10650 1250 50  0001 C CNN
F 1 "+5V" H 10665 1573 50  0000 C CNN
F 2 "" H 10650 1400 50  0001 C CNN
F 3 "" H 10650 1400 50  0001 C CNN
	1    10650 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR071
U 1 1 5DFFBA99
P 10650 3200
F 0 "#PWR071" H 10650 3050 50  0001 C CNN
F 1 "+5V" H 10665 3373 50  0000 C CNN
F 2 "" H 10650 3200 50  0001 C CNN
F 3 "" H 10650 3200 50  0001 C CNN
	1    10650 3200
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:MCP6L02T-E_SN-Codesign U6
U 1 1 5DA14767
P 9150 1950
F 0 "U6" H 9150 2317 50  0000 C CNN
F 1 "LMV358AIDR" H 9150 2226 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9150 1950 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1669460.pdf" H 9150 1950 50  0001 C CNN
F 4 "x" H 9150 1950 50  0001 C CNN "onderdelen 5mar20"
	1    9150 1950
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:MCP6L02T-E_SN-Codesign U6
U 2 1 5DA154C3
P 9150 3750
F 0 "U6" H 9150 4117 50  0000 C CNN
F 1 "LMV358AIDR" H 9150 4026 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9150 3750 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1669460.pdf" H 9150 3750 50  0001 C CNN
F 4 "x" H 9150 3750 50  0001 C CNN "onderdelen 5mar20"
	2    9150 3750
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:Ferrite_Bead-Device FB2
U 1 1 5DA2E471
P 2400 3850
F 0 "FB2" V 2126 3850 50  0000 C CNN
F 1 "Ferrite_Bead" V 2217 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2330 3850 50  0001 C CNN
F 3 "~" H 2400 3850 50  0001 C CNN
F 4 "x" H 2400 3850 50  0001 C CNN "onderdelen 5mar20"
	1    2400 3850
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:LED-Device D5
U 1 1 5DA2FEFF
P 4950 2450
F 0 "D5" V 4989 2333 50  0000 R CNN
F 1 "PWR" V 4898 2333 50  0000 R CNN
F 2 "LED_SMD:LED_PLCC_2835_Handsoldering" H 4950 2450 50  0001 C CNN
F 3 "~" H 4950 2450 50  0001 C CNN
F 4 "x" H 4950 2450 50  0001 C CNN "onderdelen 5mar20"
	1    4950 2450
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R45
U 1 1 5DA30C79
P 4700 2250
F 0 "R45" V 4493 2250 50  0000 C CNN
F 1 "1k" V 4584 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 2250 50  0001 C CNN
F 3 "~" H 4700 2250 50  0001 C CNN
F 4 "x" H 4700 2250 50  0001 C CNN "onderdelen 5mar20"
	1    4700 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 2250 4950 2300
Wire Wire Line
	4850 2250 4950 2250
Wire Wire Line
	4100 1900 3550 1900
Wire Wire Line
	4100 1850 4100 1900
Connection ~ 3550 1900
Wire Wire Line
	1450 3850 1900 3850
Wire Wire Line
	4450 3850 4450 3750
Wire Wire Line
	4000 3850 4450 3850
Connection ~ 4450 3850
Wire Wire Line
	4450 3850 4900 3850
Wire Wire Line
	4450 3450 4450 3300
Wire Wire Line
	4450 3300 4000 3300
Connection ~ 4000 3300
Wire Wire Line
	2350 3350 2350 3300
$Comp
L SemiparamEQ1-rescue:1N4148-Diode D2
U 1 1 5DBC16C3
P 3550 1700
F 0 "D2" V 3504 1779 50  0000 L CNN
F 1 "1N4148" V 3595 1779 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3550 1525 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 3550 1700 50  0001 C CNN
F 4 "x" H 3550 1700 50  0001 C CNN "onderdelen 5mar20"
	1    3550 1700
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:1N4148-Diode D3
U 1 1 5DBC1FE9
P 4450 3600
F 0 "D3" V 4404 3679 50  0000 L CNN
F 1 "1N4148" V 4495 3679 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4450 3425 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4450 3600 50  0001 C CNN
F 4 "x" H 4450 3600 50  0001 C CNN "onderdelen 5mar20"
	1    4450 3600
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:1N4148-Diode D4
U 1 1 5DBC88E6
P 4450 2450
F 0 "D4" V 4404 2529 50  0000 L CNN
F 1 "1N4148" V 4495 2529 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4450 2275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4450 2450 50  0001 C CNN
F 4 "x" H 4450 2450 50  0001 C CNN "onderdelen 5mar20"
	1    4450 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 2650 4450 2600
Wire Wire Line
	4450 2300 4450 2250
Wire Wire Line
	6600 5050 6600 5450
Wire Wire Line
	6350 5450 6600 5450
Connection ~ 6600 5450
$Comp
L SemiparamEQ1-rescue:PWR_FLAG-power #FLG02
U 1 1 5DBE9B58
P 1700 2600
F 0 "#FLG02" H 1700 2675 50  0001 C CNN
F 1 "PWR_FLAG" H 1700 2773 50  0000 C CNN
F 2 "" H 1700 2600 50  0001 C CNN
F 3 "~" H 1700 2600 50  0001 C CNN
	1    1700 2600
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:PWR_FLAG-power #FLG03
U 1 1 5DBEA6AC
P 2500 1250
F 0 "#FLG03" H 2500 1325 50  0001 C CNN
F 1 "PWR_FLAG" H 2500 1423 50  0000 C CNN
F 2 "" H 2500 1250 50  0001 C CNN
F 3 "~" H 2500 1250 50  0001 C CNN
	1    2500 1250
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:PWR_FLAG-power #FLG01
U 1 1 5DBEA82D
P 1900 4100
F 0 "#FLG01" H 1900 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 1900 4273 50  0000 C CNN
F 2 "" H 1900 4100 50  0001 C CNN
F 3 "~" H 1900 4100 50  0001 C CNN
	1    1900 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 1250 2500 1500
Connection ~ 2500 1500
Wire Wire Line
	2500 1500 2650 1500
Connection ~ 1900 3850
Wire Wire Line
	1900 3850 2250 3850
Wire Wire Line
	1900 3850 1900 4100
Wire Wire Line
	4450 2650 4950 2650
Wire Wire Line
	4450 2250 4550 2250
Wire Wire Line
	4950 2600 4950 2650
$Comp
L SemiparamEQ1-rescue:TestPoint-Connector TP12
U 1 1 5DC99760
P 5750 3300
F 0 "TP12" H 5808 3418 50  0000 L CNN
F 1 "GND" H 5808 3327 50  0000 L CNN
F 2 "Gfx_Semiparameq:spiceboys" H 5950 3300 50  0001 C CNN
F 3 "~" H 5950 3300 50  0001 C CNN
F 4 "x" H 5750 3300 50  0001 C CNN "onderdelen 5mar20"
	1    5750 3300
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR055
U 1 1 5DC99D8F
P 5750 3500
F 0 "#PWR055" H 5750 3250 50  0001 C CNN
F 1 "GND" H 5755 3327 50  0000 C CNN
F 2 "" H 5750 3500 50  0001 C CNN
F 3 "" H 5750 3500 50  0001 C CNN
	1    5750 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1500 1950 1500
$Comp
L SemiparamEQ1-rescue:Conn_01x03_Male-Connector J4
U 1 1 5DCBD2FE
P 1000 2600
F 0 "J4" H 1108 2881 50  0000 C CNN
F 1 "VCC_GND_VEE" H 1108 2790 50  0000 C CNN
F 2 "Connector:Banana_Jack_3Pin" H 1000 2600 50  0001 C CNN
F 3 "~" H 1000 2600 50  0001 C CNN
F 4 "x" H 1000 2600 50  0001 C CNN "onderdelen 5mar20"
	1    1000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2600 1200 2600
Wire Wire Line
	1200 2700 1450 2700
Wire Wire Line
	1450 2700 1450 3850
Wire Wire Line
	1200 2500 1350 2500
Wire Wire Line
	1350 2500 1350 1500
Wire Wire Line
	6600 5450 6750 5450
Wire Wire Line
	7000 5550 7000 5950
Wire Wire Line
	7000 5350 6850 5350
Wire Wire Line
	6850 5350 6850 5650
Wire Wire Line
	7000 5950 6850 5950
Connection ~ 7000 5950
Wire Wire Line
	7000 5950 7000 6050
Wire Wire Line
	8450 1850 8450 3650
Wire Wire Line
	8450 1150 8450 1300
Wire Wire Line
	8450 1600 8450 1650
Connection ~ 8450 1650
$Comp
L SemiparamEQ1-rescue:TestPoint_Probe-Connector TP14
U 1 1 5DD3DF4B
P 8050 1250
F 0 "TP14" H 8203 1351 50  0000 L CNN
F 1 "VinAC" H 8100 1250 50  0000 L CNN
F 2 "Codesign:TestPoint_Pad_3.4x4.7mm" H 8250 1250 50  0001 C CNN
F 3 "~" H 8250 1250 50  0001 C CNN
F 4 "x" H 8050 1250 50  0001 C CNN "onderdelen 5mar20"
	1    8050 1250
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:1N4148-Diode D6
U 1 1 5DD9112D
P 9600 2350
F 0 "D6" H 9600 2134 50  0000 C CNN
F 1 "1N4148" H 9600 2225 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9600 2175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9600 2350 50  0001 C CNN
F 4 "x" H 9600 2350 50  0001 C CNN "onderdelen 5mar20"
	1    9600 2350
	-1   0    0    1   
$EndComp
Connection ~ 9750 2350
Connection ~ 9450 2350
$Comp
L SemiparamEQ1-rescue:1N4148-Diode D7
U 1 1 5DD91DC2
P 9600 4150
F 0 "D7" H 9600 3934 50  0000 C CNN
F 1 "1N4148" H 9600 4025 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 9600 3975 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 9600 4150 50  0001 C CNN
F 4 "x" H 9600 4150 50  0001 C CNN "onderdelen 5mar20"
	1    9600 4150
	-1   0    0    1   
$EndComp
Connection ~ 9750 4150
Connection ~ 9450 4150
$Comp
L SemiparamEQ1-rescue:Conn_01x02_Male-Connector J5
U 1 1 5DD95A3B
P 5350 3350
F 0 "J5" H 5458 3531 50  0000 C CNN
F 1 "GND" H 5458 3440 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 5350 3350 50  0001 C CNN
F 3 "~" H 5350 3350 50  0001 C CNN
F 4 "x" H 5350 3350 50  0001 C CNN "onderdelen 5mar20"
	1    5350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3350 5550 3400
Wire Wire Line
	5750 3500 5750 3400
Wire Wire Line
	5550 3400 5750 3400
Connection ~ 5550 3400
Wire Wire Line
	5550 3400 5550 3450
Connection ~ 5750 3400
Wire Wire Line
	5750 3400 5750 3300
$Comp
L SemiparamEQ1-rescue:MCP6L02T-E_SN-Codesign U?
U 3 1 5DF5F004
P 10400 5750
AR Path="/5DB4A226/5DF5F004" Ref="U?"  Part="3" 
AR Path="/5DCEAD28/5DF5F004" Ref="U6"  Part="3" 
F 0 "U6" H 10358 5796 50  0000 L CNN
F 1 "LMV358AIDR" H 10358 5705 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 10400 5750 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1669460.pdf" H 10400 5750 50  0001 C CNN
F 4 "x" H 10400 5750 50  0001 C CNN "onderdelen 5mar20"
	3    10400 5750
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C?
U 1 1 5DF5F00A
P 10000 5750
AR Path="/5DB4A226/5DF5F00A" Ref="C?"  Part="1" 
AR Path="/5DCEAD28/5DF5F00A" Ref="C28"  Part="1" 
F 0 "C28" V 9748 5750 50  0000 C CNN
F 1 "100n" V 9839 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10038 5600 50  0001 C CNN
F 3 "~" H 10000 5750 50  0001 C CNN
F 4 "x" H 10000 5750 50  0001 C CNN "onderdelen 5mar20"
	1    10000 5750
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5DF5F010
P 10300 6050
AR Path="/5DF5F010" Ref="#PWR?"  Part="1" 
AR Path="/5DB4A226/5DF5F010" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5DF5F010" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 10300 5800 50  0001 C CNN
F 1 "GND" H 10305 5877 50  0000 C CNN
F 2 "" H 10300 6050 50  0001 C CNN
F 3 "" H 10300 6050 50  0001 C CNN
	1    10300 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 6050 10000 6050
Wire Wire Line
	10000 6050 10000 5900
Connection ~ 10300 6050
Wire Wire Line
	10000 5600 10000 5450
Wire Wire Line
	10000 5450 10300 5450
$Comp
L SemiparamEQ1-rescue:+5V-power #PWR?
U 1 1 5DF5F01B
P 10300 5450
AR Path="/5DB4A226/5DF5F01B" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5DF5F01B" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 10300 5300 50  0001 C CNN
F 1 "+5V" H 10315 5623 50  0000 C CNN
F 2 "" H 10300 5450 50  0001 C CNN
F 3 "" H 10300 5450 50  0001 C CNN
	1    10300 5450
	1    0    0    -1  
$EndComp
Connection ~ 10300 5450
Wire Wire Line
	6050 5450 5800 5450
Text Notes 9600 3600 0    50   ~ 0
Signal ON LED (ghetto)
Text Notes 9600 1800 0    50   ~ 0
Signal CLIP LED (ghetto)
$Comp
L SemiparamEQ1-rescue:R-Device R52
U 1 1 5DF02507
P 10650 1550
F 0 "R52" H 10720 1596 50  0000 L CNN
F 1 "300" H 10720 1505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10580 1550 50  0001 C CNN
F 3 "~" H 10650 1550 50  0001 C CNN
F 4 "x" H 10650 1550 50  0001 C CNN "onderdelen 5mar20"
	1    10650 1550
	1    0    0    -1  
$EndComp
Connection ~ 4450 2650
Connection ~ 4450 2250
Wire Wire Line
	3350 2250 3700 2250
Wire Wire Line
	4000 2250 4150 2250
Connection ~ 3700 2250
Wire Wire Line
	3700 2250 4000 2250
Connection ~ 4150 2250
Wire Wire Line
	4150 2250 4450 2250
Text Notes 1000 2850 0    50   ~ 0
-15V
$Comp
L Jumper:Jumper_3_Open JP?
U 1 1 5E5AFF55
P 4950 1750
AR Path="/5DB4A226/5E5AFF55" Ref="JP?"  Part="1" 
AR Path="/5DCEAD28/5E5AFF55" Ref="JP2"  Part="1" 
F 0 "JP2" H 4800 1900 50  0000 L CNN
F 1 "Jumper_3_Open" H 5000 1900 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4950 1750 50  0001 C CNN
F 3 "~" H 4950 1750 50  0001 C CNN
F 4 "x" H 4950 1750 50  0001 C CNN "onderdelen 5mar20"
	1    4950 1750
	1    0    0    1   
$EndComp
Wire Wire Line
	4150 2250 4150 1950
Text HLabel 5400 1750 2    50   Input ~ 0
VRAW
Wire Wire Line
	5400 1750 5200 1750
Text HLabel 4700 1500 0    50   Input ~ 0
VinXmega
Wire Wire Line
	2550 3850 2750 3850
Wire Wire Line
	2350 3300 2750 3300
$Comp
L Regulator_Linear:LDK130-08_SOT23_SOT353 U9
U 1 1 5E615702
P 3000 1500
F 0 "U9" H 3000 1842 50  0000 C CNN
F 1 "LDK320AM120R" H 3000 1751 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 3000 1825 50  0001 C CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/29/10/f7/87/2f/66/47/f4/DM00076097.pdf/files/DM00076097.pdf/jcr:content/translations/en.DM00076097.pdf" H 3000 1500 50  0001 C CNN
F 4 "x" H 3000 1500 50  0001 C CNN "onderdelen 5mar20"
	1    3000 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1500 2650 1400
Wire Wire Line
	2650 1400 2700 1400
Wire Wire Line
	3300 1400 3550 1400
Connection ~ 3550 1400
Wire Wire Line
	3550 1400 3550 1550
Wire Wire Line
	4100 1400 4100 1550
$Comp
L power:+12V #PWR0117
U 1 1 5E6755F3
P 4100 1400
F 0 "#PWR0117" H 4100 1250 50  0001 C CNN
F 1 "+12V" H 4115 1573 50  0000 C CNN
F 2 "" H 4100 1400 50  0001 C CNN
F 3 "" H 4100 1400 50  0001 C CNN
	1    4100 1400
	1    0    0    -1  
$EndComp
Connection ~ 4100 1400
$Comp
L power:-12V #PWR0118
U 1 1 5E6760B0
P 4900 3700
F 0 "#PWR0118" H 4900 3800 50  0001 C CNN
F 1 "-12V" H 4915 3873 50  0000 C CNN
F 2 "" H 4900 3700 50  0001 C CNN
F 3 "" H 4900 3700 50  0001 C CNN
	1    4900 3700
	1    0    0    -1  
$EndComp
Text Notes 1000 2200 0    50   ~ 0
+15V
Connection ~ 1700 2600
$Comp
L Device:CP1 C19
U 1 1 5E61D82D
P 2350 1650
F 0 "C19" H 2465 1696 50  0000 L CNN
F 1 "1u" H 2465 1605 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2350 1650 50  0001 C CNN
F 3 "~" H 2350 1650 50  0001 C CNN
F 4 "x" H 2350 1650 50  0001 C CNN "onderdelen 5mar20"
	1    2350 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1500 2350 1500
Connection ~ 2350 1500
Wire Wire Line
	2350 1500 2500 1500
$Comp
L SemiparamEQ1-rescue:CP1-Device C22
U 1 1 5E621E91
P 4000 2500
F 0 "C22" H 4115 2546 50  0000 L CNN
F 1 "2u2" H 4115 2455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 4000 2500 50  0001 C CNN
F 3 "~" H 4000 2500 50  0001 C CNN
F 4 "x" H 4000 2500 50  0001 C CNN "onderdelen 5mar20"
	1    4000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2650 4000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 4450 2650
$Comp
L SemiparamEQ1-rescue:CP1-Device C20
U 1 1 5E623B31
P 2750 3550
F 0 "C20" H 2865 3596 50  0000 L CNN
F 1 "10u" H 2865 3505 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2750 3550 50  0001 C CNN
F 3 "~" H 2750 3550 50  0001 C CNN
F 4 "x" H 2750 3550 50  0001 C CNN "onderdelen 5mar20"
	1    2750 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 3300 2750 3400
Connection ~ 2750 3300
Wire Wire Line
	2750 3300 3350 3300
Wire Wire Line
	2750 3700 2750 3850
Connection ~ 2750 3850
Wire Wire Line
	2750 3850 2950 3850
$Comp
L SemiparamEQ1-rescue:R-Device R48
U 1 1 5DE3D00A
P 8850 2600
F 0 "R48" H 8780 2554 50  0000 R CNN
F 1 "10k" H 8780 2645 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8780 2600 50  0001 C CNN
F 3 "~" H 8850 2600 50  0001 C CNN
F 4 "x" H 8850 2600 50  0001 C CNN "onderdelen 5mar20"
	1    8850 2600
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:MC001294_AudioJack3_SwitchTR-Codesign J12
U 1 1 5E66DA7F
P 7200 5450
F 0 "J12" H 6920 5283 50  0000 R CNN
F 1 "TRSout" H 6920 5374 50  0000 R CNN
F 2 "Codesign:Jack_3.5mm_MC001294_5pin_Multicomp" H 7200 5450 50  0001 C CNN
F 3 "~" H 7200 5450 50  0001 C CNN
F 4 "x" H 7200 5450 50  0001 C CNN "onderdelen 5mar20"
	1    7200 5450
	-1   0    0    1   
$EndComp
Wire Wire Line
	6750 5450 6750 5250
Wire Wire Line
	6750 5250 7000 5250
Connection ~ 6750 5450
Wire Wire Line
	6750 5450 7000 5450
Wire Wire Line
	7000 5150 6850 5150
Wire Wire Line
	6850 5150 6850 5350
Connection ~ 6850 5350
Wire Wire Line
	4150 1950 4700 1950
Wire Wire Line
	4700 1950 4700 1750
Connection ~ 4700 1750
Wire Wire Line
	4700 1750 4700 1500
$Comp
L SemiparamEQ1-rescue:Conn_01x02-Connector_Generic J7
U 1 1 5DD7BF53
P 6500 1050
F 0 "J7" H 6418 1267 50  0000 C CNN
F 1 "Vin" H 6418 1176 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 6500 1050 50  0001 C CNN
F 3 "~" H 6500 1050 50  0001 C CNN
F 4 "x" H 6500 1050 50  0001 C CNN "onderdelen 5mar20"
	1    6500 1050
	-1   0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:MC001294_AudioJack3_SwitchTR-Codesign J11
U 1 1 5E66F95D
P 6500 1750
F 0 "J11" H 6221 1583 50  0000 R CNN
F 1 "TRSin" H 6221 1674 50  0000 R CNN
F 2 "Codesign:Jack_3.5mm_MC001294_5pin_Multicomp" H 6500 1750 50  0001 C CNN
F 3 "~" H 6500 1750 50  0001 C CNN
F 4 "x" H 6500 1750 50  0001 C CNN "onderdelen 5mar20"
	1    6500 1750
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U11
U 1 1 5E6A3477
P 7650 1650
AR Path="/5E6A3477" Ref="U11"  Part="1" 
AR Path="/5DCEAD28/5E6A3477" Ref="U11"  Part="1" 
F 0 "U11" H 7600 1850 50  0000 C CNN
F 1 "LM833" H 7650 1950 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 7650 1650 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 7650 1650 50  0001 C CNN
F 4 "x" H 7650 1650 50  0001 C CNN "onderdelen 5mar20"
	1    7650 1650
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U11
U 2 1 5E6A468E
P 4050 5900
AR Path="/5E6A468E" Ref="U11"  Part="2" 
AR Path="/5DCEAD28/5E6A468E" Ref="U11"  Part="2" 
F 0 "U11" H 4050 5533 50  0000 C CNN
F 1 "LM833" H 4050 5624 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 4050 5900 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 4050 5900 50  0001 C CNN
F 4 "x" H 4050 5900 50  0001 C CNN "onderdelen 5mar20"
	2    4050 5900
	1    0    0    1   
$EndComp
Wire Wire Line
	7950 1650 8050 1650
Wire Wire Line
	8050 1250 8050 1650
Connection ~ 8050 1650
Wire Wire Line
	8050 1650 8450 1650
$Comp
L Device:R R61
U 1 1 5E6D43AE
P 7650 1250
F 0 "R61" V 7443 1250 50  0000 C CNN
F 1 "10k" V 7534 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7580 1250 50  0001 C CNN
F 3 "~" H 7650 1250 50  0001 C CNN
	1    7650 1250
	0    1    1    0   
$EndComp
$Comp
L Device:R R59
U 1 1 5E6D4A85
P 7000 1550
F 0 "R59" V 6793 1550 50  0000 C CNN
F 1 "10k" V 6884 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6930 1550 50  0001 C CNN
F 3 "~" H 7000 1550 50  0001 C CNN
	1    7000 1550
	0    1    1    0   
$EndComp
$Comp
L Device:R R60
U 1 1 5E6D4E1C
P 7000 1750
F 0 "R60" V 7100 1750 50  0000 C CNN
F 1 "10k" V 7200 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6930 1750 50  0001 C CNN
F 3 "~" H 7000 1750 50  0001 C CNN
	1    7000 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	7150 1750 7150 1550
Wire Wire Line
	7350 1550 7250 1550
Connection ~ 7150 1550
$Comp
L power:GND #PWR016
U 1 1 5E6E1462
P 7350 1850
F 0 "#PWR016" H 7350 1600 50  0001 C CNN
F 1 "GND" H 7355 1677 50  0000 C CNN
F 2 "" H 7350 1850 50  0001 C CNN
F 3 "" H 7350 1850 50  0001 C CNN
	1    7350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1850 7350 1750
Wire Wire Line
	7250 1550 7250 1250
Wire Wire Line
	7250 1250 7500 1250
Connection ~ 7250 1550
Wire Wire Line
	7250 1550 7150 1550
Wire Wire Line
	7800 1250 7950 1250
Wire Wire Line
	7950 1250 7950 1650
Connection ~ 7950 1650
Wire Wire Line
	6700 1750 6850 1750
Wire Wire Line
	6700 1550 6850 1550
$Comp
L power:GND #PWR014
U 1 1 5E703666
P 6700 1950
F 0 "#PWR014" H 6700 1700 50  0001 C CNN
F 1 "GND" H 6705 1777 50  0000 C CNN
F 2 "" H 6700 1950 50  0001 C CNN
F 3 "" H 6700 1950 50  0001 C CNN
	1    6700 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1950 6700 1850
Connection ~ 6700 1850
Wire Wire Line
	6700 1650 6750 1650
Wire Wire Line
	6750 1650 6750 1850
Wire Wire Line
	6750 1850 6700 1850
Wire Wire Line
	6700 1150 6700 1450
Wire Wire Line
	6700 1050 6700 1150
Connection ~ 6700 1150
$Comp
L Device:R R57
U 1 1 5E7CDF78
P 4200 5400
F 0 "R57" V 3993 5400 50  0000 C CNN
F 1 "40k" V 4084 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4130 5400 50  0001 C CNN
F 3 "~" H 4200 5400 50  0001 C CNN
F 4 "x" H 4200 5400 50  0001 C CNN "onderdelen 5mar20"
	1    4200 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R R56
U 1 1 5E7CE978
P 3500 5800
F 0 "R56" V 3293 5800 50  0000 C CNN
F 1 "10k" V 3384 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3430 5800 50  0001 C CNN
F 3 "~" H 3500 5800 50  0001 C CNN
	1    3500 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 5800 3700 5800
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U?
U 3 1 5E7D98FC
P 9500 5750
AR Path="/5E7D98FC" Ref="U?"  Part="3" 
AR Path="/5DCEAD28/5E7D98FC" Ref="U11"  Part="3" 
F 0 "U11" H 9458 5796 50  0000 L CNN
F 1 "LM833" H 9458 5705 50  0000 L CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 9500 5750 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 9500 5750 50  0001 C CNN
F 4 "x" H 9500 5750 50  0001 C CNN "onderdelen 5mar20"
	3    9500 5750
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E7D9902
P 8950 5750
AR Path="/5E7D9902" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5E7D9902" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 8950 5500 50  0001 C CNN
F 1 "GND" H 8955 5577 50  0000 C CNN
F 2 "" H 8950 5750 50  0001 C CNN
F 3 "" H 8950 5750 50  0001 C CNN
	1    8950 5750
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C?
U 1 1 5E7D9909
P 9250 5450
AR Path="/5E7D9909" Ref="C?"  Part="1" 
AR Path="/5DCEAD28/5E7D9909" Ref="C25"  Part="1" 
F 0 "C25" V 8998 5450 50  0000 C CNN
F 1 "100n" V 9089 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9288 5300 50  0001 C CNN
F 3 "~" H 9250 5450 50  0001 C CNN
F 4 "x" H 9250 5450 50  0001 C CNN "onderdelen 5mar20"
	1    9250 5450
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C?
U 1 1 5E7D9910
P 9250 6050
AR Path="/5E7D9910" Ref="C?"  Part="1" 
AR Path="/5DCEAD28/5E7D9910" Ref="C33"  Part="1" 
F 0 "C33" V 8998 6050 50  0000 C CNN
F 1 "100n" V 9089 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9288 5900 50  0001 C CNN
F 3 "~" H 9250 6050 50  0001 C CNN
F 4 "x" H 9250 6050 50  0001 C CNN "onderdelen 5mar20"
	1    9250 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	9100 5750 8950 5750
Connection ~ 9100 5750
Wire Wire Line
	9100 5450 9100 5750
Wire Wire Line
	9100 5750 9100 6050
Wire Wire Line
	9400 5400 9400 5450
Connection ~ 9400 5450
Wire Wire Line
	9400 6100 9400 6050
Connection ~ 9400 6050
$Comp
L power:+12V #PWR?
U 1 1 5E7D991E
P 9400 5400
AR Path="/5E7D991E" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5E7D991E" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 9400 5250 50  0001 C CNN
F 1 "+12V" H 9415 5573 50  0000 C CNN
F 2 "" H 9400 5400 50  0001 C CNN
F 3 "" H 9400 5400 50  0001 C CNN
	1    9400 5400
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR?
U 1 1 5E7D9924
P 9400 6100
AR Path="/5E7D9924" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5E7D9924" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 9400 6200 50  0001 C CNN
F 1 "-12V" H 9415 6273 50  0000 C CNN
F 2 "" H 9400 6100 50  0001 C CNN
F 3 "" H 9400 6100 50  0001 C CNN
	1    9400 6100
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E7F1146
P 3750 6150
AR Path="/5E7F1146" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5E7F1146" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 3750 5900 50  0001 C CNN
F 1 "GND" H 3755 5977 50  0000 C CNN
F 2 "" H 3750 6150 50  0001 C CNN
F 3 "" H 3750 6150 50  0001 C CNN
	1    3750 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 6150 3750 6000
Text HLabel 5150 5900 2    50   Output ~ 0
SAMout
Wire Wire Line
	4650 5900 4450 5900
Wire Wire Line
	3700 5800 3700 5400
Wire Wire Line
	3700 5400 4050 5400
Connection ~ 3700 5800
Wire Wire Line
	3700 5800 3750 5800
Wire Wire Line
	4350 5400 4450 5400
Wire Wire Line
	4450 5400 4450 5900
Connection ~ 4450 5900
Wire Wire Line
	4450 5900 4350 5900
$Comp
L Device:R R?
U 1 1 5E83AB5F
P 2050 5650
AR Path="/5E83AB5F" Ref="R?"  Part="1" 
AR Path="/5DCEAD28/5E83AB5F" Ref="R54"  Part="1" 
F 0 "R54" H 2120 5696 50  0000 L CNN
F 1 "30k" H 2120 5605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1980 5650 50  0001 C CNN
F 3 "~" H 2050 5650 50  0001 C CNN
F 4 "x" H 2050 5650 50  0001 C CNN "onderdelen 5mar20"
	1    2050 5650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E83AB65
P 2050 6050
AR Path="/5E83AB65" Ref="R?"  Part="1" 
AR Path="/5DCEAD28/5E83AB65" Ref="R55"  Part="1" 
F 0 "R55" H 2120 6096 50  0000 L CNN
F 1 "10k" H 2120 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1980 6050 50  0001 C CNN
F 3 "~" H 2050 6050 50  0001 C CNN
	1    2050 6050
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E83AB6B
P 2050 6250
AR Path="/5E83AB6B" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5E83AB6B" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 2050 6000 50  0001 C CNN
F 1 "GND" H 2055 6077 50  0000 C CNN
F 2 "" H 2050 6250 50  0001 C CNN
F 3 "" H 2050 6250 50  0001 C CNN
	1    2050 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6250 2050 6200
Wire Wire Line
	2050 5500 2050 5400
$Comp
L SemiparamEQ1-rescue:Conn_01x03_Male-Connector J?
U 1 1 5E83AB73
P 2850 5600
AR Path="/5E83AB73" Ref="J?"  Part="1" 
AR Path="/5DCEAD28/5E83AB73" Ref="J6"  Part="1" 
F 0 "J6" V 2700 5700 50  0000 R CNN
F 1 "SAM" V 2800 5700 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x03_P1.27mm_Vertical" H 2850 5600 50  0001 C CNN
F 3 "" H 2850 5600 50  0001 C CNN
	1    2850 5600
	0    -1   1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E83AB79
P 2850 5950
AR Path="/5E83AB79" Ref="#PWR?"  Part="1" 
AR Path="/5DCEAD28/5E83AB79" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 2850 5700 50  0001 C CNN
F 1 "GND" H 2855 5777 50  0000 C CNN
F 2 "" H 2850 5950 50  0001 C CNN
F 3 "" H 2850 5950 50  0001 C CNN
	1    2850 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 5950 2850 5800
Text HLabel 2050 5400 0    50   Input ~ 0
SAMin
Text Notes 2350 5250 0    50   ~ 0
/4 going into SAM\n*4 coming out of SAM
$Comp
L Device:R R58
U 1 1 5E88A55B
P 4800 5900
F 0 "R58" V 4593 5900 50  0000 C CNN
F 1 "10k" V 4684 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4730 5900 50  0001 C CNN
F 3 "~" H 4800 5900 50  0001 C CNN
	1    4800 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 5900 4950 5900
Wire Wire Line
	2950 5800 3350 5800
Wire Wire Line
	2050 5800 2050 5900
Wire Wire Line
	2750 5800 2050 5800
Connection ~ 2050 5800
$EndSCHEMATC
