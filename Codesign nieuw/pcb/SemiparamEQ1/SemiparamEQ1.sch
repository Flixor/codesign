EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Single band semiparametric EQ"
Date ""
Rev ""
Comp "Flixor Studios"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SemiparamEQ1-rescue:R-Device R13
U 1 1 5D8E35AC
P 7000 2750
F 0 "R13" V 7207 2750 50  0000 C CNN
F 1 "10k" V 7116 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6930 2750 50  0001 C CNN
F 3 "~" H 7000 2750 50  0001 C CNN
F 4 "x" H 7000 2750 50  0001 C CNN "onderdelen 5mar20"
	1    7000 2750
	0    -1   -1   0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R14
U 1 1 5D8F3514
P 8050 2250
F 0 "R14" V 7843 2250 50  0000 C CNN
F 1 "10k" V 7934 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7980 2250 50  0001 C CNN
F 3 "~" H 8050 2250 50  0001 C CNN
F 4 "x" H 8050 2250 50  0001 C CNN "onderdelen 5mar20"
	1    8050 2250
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R16
U 1 1 5D8F3BDB
P 9000 2850
F 0 "R16" V 8793 2850 50  0000 C CNN
F 1 "10k" V 8884 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8930 2850 50  0001 C CNN
F 3 "~" H 9000 2850 50  0001 C CNN
F 4 "x" H 9000 2850 50  0001 C CNN "onderdelen 5mar20"
	1    9000 2850
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R17
U 1 1 5D8F3F36
P 9850 2400
F 0 "R17" V 9643 2400 50  0000 C CNN
F 1 "10k" V 9734 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9780 2400 50  0001 C CNN
F 3 "~" H 9850 2400 50  0001 C CNN
F 4 "x" H 9850 2400 50  0001 C CNN "onderdelen 5mar20"
	1    9850 2400
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR012
U 1 1 5D8F5DF5
P 9400 3250
F 0 "#PWR012" H 9400 3000 50  0001 C CNN
F 1 "GND" H 9405 3077 50  0000 C CNN
F 2 "" H 9400 3250 50  0001 C CNN
F 3 "" H 9400 3250 50  0001 C CNN
	1    9400 3250
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR05
U 1 1 5D8F6212
P 7550 3100
F 0 "#PWR05" H 7550 2850 50  0001 C CNN
F 1 "GND" H 7555 2927 50  0000 C CNN
F 2 "" H 7550 3100 50  0001 C CNN
F 3 "" H 7550 3100 50  0001 C CNN
	1    7550 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 2750 7350 2750
Wire Wire Line
	7650 2950 7550 2950
Wire Wire Line
	7550 2950 7550 3100
Wire Wire Line
	10100 2950 10200 2950
Wire Wire Line
	9500 3050 9400 3050
Wire Wire Line
	9400 3050 9400 3250
Wire Wire Line
	9400 2850 9400 2400
Wire Wire Line
	9400 2400 9700 2400
Connection ~ 9400 2850
Wire Wire Line
	9400 2850 9500 2850
Wire Wire Line
	10000 2400 10200 2400
Wire Wire Line
	10200 2400 10200 2950
Wire Wire Line
	8500 2850 8500 2250
Wire Wire Line
	8500 2250 8200 2250
Connection ~ 8500 2850
Wire Wire Line
	8500 2850 8850 2850
Wire Wire Line
	7900 2250 7500 2250
Wire Wire Line
	7500 2250 7500 2750
Connection ~ 7500 2750
Wire Wire Line
	7500 2750 7650 2750
Wire Wire Line
	8250 2850 8350 2850
Wire Wire Line
	8350 3200 8350 2850
Connection ~ 8350 2850
Wire Wire Line
	8350 2850 8500 2850
Text GLabel 8750 3200 2    50   Input ~ 0
BPin
Text GLabel 6850 2750 0    50   Input ~ 0
V_in
$Comp
L SemiparamEQ1-rescue:R-Device R1
U 1 1 5D952714
P 1850 5100
F 0 "R1" V 1643 5100 50  0000 C CNN
F 1 "22k" V 1734 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1780 5100 50  0001 C CNN
F 3 "~" H 1850 5100 50  0001 C CNN
F 4 "x" H 1850 5100 50  0001 C CNN "onderdelen 5mar20"
	1    1850 5100
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R2
U 1 1 5D953286
P 1850 5400
F 0 "R2" V 1643 5400 50  0000 C CNN
F 1 "500k" V 1734 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1780 5400 50  0001 C CNN
F 3 "~" H 1850 5400 50  0001 C CNN
F 4 "x" H 1850 5400 50  0001 C CNN "onderdelen 5mar20"
	1    1850 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	1550 5400 1700 5400
Connection ~ 1550 5100
Wire Wire Line
	1550 5100 1550 5400
Wire Wire Line
	1550 5100 1700 5100
Wire Wire Line
	2350 5100 2250 5100
$Comp
L SemiparamEQ1-rescue:R-Device R5
U 1 1 5D9618E3
P 2700 4650
F 0 "R5" V 2493 4650 50  0000 C CNN
F 1 "10k" V 2584 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2630 4650 50  0001 C CNN
F 3 "~" H 2700 4650 50  0001 C CNN
F 4 "x" H 2700 4650 50  0001 C CNN "onderdelen 5mar20"
	1    2700 4650
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R7
U 1 1 5D961B97
P 3000 4150
F 0 "R7" V 2793 4150 50  0000 C CNN
F 1 "100k" V 2884 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2930 4150 50  0001 C CNN
F 3 "~" H 3000 4150 50  0001 C CNN
F 4 "x" H 3000 4150 50  0001 C CNN "onderdelen 5mar20"
	1    3000 4150
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R6
U 1 1 5D961DD5
P 2950 5700
F 0 "R6" V 2743 5700 50  0000 C CNN
F 1 "100k" V 2834 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2880 5700 50  0001 C CNN
F 3 "~" H 2950 5700 50  0001 C CNN
F 4 "x" H 2950 5700 50  0001 C CNN "onderdelen 5mar20"
	1    2950 5700
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R3
U 1 1 5D962527
P 2100 6050
F 0 "R3" H 2030 6004 50  0000 R CNN
F 1 "12k" H 2030 6095 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2030 6050 50  0001 C CNN
F 3 "~" H 2100 6050 50  0001 C CNN
F 4 "x" H 2100 6050 50  0001 C CNN "onderdelen 5mar20"
	1    2100 6050
	-1   0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R4
U 1 1 5D962C7A
P 2400 6050
F 0 "R4" H 2330 6004 50  0000 R CNN
F 1 "160k" H 2330 6095 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2330 6050 50  0001 C CNN
F 3 "~" H 2400 6050 50  0001 C CNN
F 4 "x" H 2400 6050 50  0001 C CNN "onderdelen 5mar20"
	1    2400 6050
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 5900 2250 5900
Wire Wire Line
	2100 6200 2250 6200
$Comp
L SemiparamEQ1-rescue:GND-power #PWR01
U 1 1 5D967D4B
P 2250 6200
F 0 "#PWR01" H 2250 5950 50  0001 C CNN
F 1 "GND" H 2255 6027 50  0000 C CNN
F 2 "" H 2250 6200 50  0001 C CNN
F 3 "" H 2250 6200 50  0001 C CNN
	1    2250 6200
	1    0    0    -1  
$EndComp
Connection ~ 2250 6200
Wire Wire Line
	2250 6200 2400 6200
Connection ~ 2250 5900
Wire Wire Line
	2250 5900 2400 5900
Wire Wire Line
	2350 5300 2250 5300
Wire Wire Line
	2250 5300 2250 5700
Wire Wire Line
	2950 5200 3050 5200
Wire Wire Line
	3050 5200 3050 4650
Wire Wire Line
	3050 4650 2850 4650
Wire Wire Line
	2250 5100 2250 4650
Wire Wire Line
	2250 4650 2550 4650
Connection ~ 3050 5200
Wire Wire Line
	4000 5150 4000 5350
Wire Wire Line
	4000 5350 4650 5350
Wire Wire Line
	4650 5350 4650 5050
Wire Wire Line
	4650 5050 4600 5050
Wire Wire Line
	2250 5700 2800 5700
Connection ~ 2250 5700
Wire Wire Line
	2250 5700 2250 5900
Wire Wire Line
	5500 4150 5500 4300
Wire Wire Line
	5500 4300 4900 4300
Wire Wire Line
	4900 4300 4900 4050
Connection ~ 4650 5050
Connection ~ 6300 5150
Wire Wire Line
	6300 5700 6300 5150
Wire Wire Line
	5550 5250 5550 5300
Wire Wire Line
	5650 5250 5550 5250
$Comp
L SemiparamEQ1-rescue:GND-power #PWR03
U 1 1 5D9AAB32
P 5550 5300
F 0 "#PWR03" H 5550 5050 50  0001 C CNN
F 1 "GND" H 5555 5127 50  0000 C CNN
F 2 "" H 5550 5300 50  0001 C CNN
F 3 "" H 5550 5300 50  0001 C CNN
	1    5550 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5050 5650 5050
Connection ~ 5400 5050
Wire Wire Line
	5400 4600 5600 4600
Wire Wire Line
	5400 5050 5400 4600
Wire Wire Line
	6300 4600 5900 4600
Wire Wire Line
	6300 5150 6300 5000
Wire Wire Line
	6250 5150 6300 5150
$Comp
L SemiparamEQ1-rescue:C-Device C3
U 1 1 5D9A1E5B
P 5750 4600
F 0 "C3" V 5498 4600 50  0000 C CNN
F 1 "1000p" V 5589 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5788 4450 50  0001 C CNN
F 3 "~" H 5750 4600 50  0001 C CNN
F 4 "x" H 5750 4600 50  0001 C CNN "onderdelen 5mar20"
	1    5750 4600
	0    1    1    0   
$EndComp
Connection ~ 5150 5050
Wire Wire Line
	5150 5050 5400 5050
Wire Wire Line
	5150 5350 5150 5050
Connection ~ 4850 5050
Wire Wire Line
	4850 5350 4850 5050
Wire Wire Line
	4650 5050 4850 5050
$Comp
L SemiparamEQ1-rescue:R-Device R11
U 1 1 5D99251C
P 5000 5350
F 0 "R11" V 4793 5350 50  0000 C CNN
F 1 "390k" V 4884 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4930 5350 50  0001 C CNN
F 3 "~" H 5000 5350 50  0001 C CNN
F 4 "x" H 5000 5350 50  0001 C CNN "onderdelen 5mar20"
	1    5000 5350
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R10
U 1 1 5D991FD1
P 5000 5050
F 0 "R10" V 4793 5050 50  0000 C CNN
F 1 "13k" V 4884 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4930 5050 50  0001 C CNN
F 3 "~" H 5000 5050 50  0001 C CNN
F 4 "x" H 5000 5050 50  0001 C CNN "onderdelen 5mar20"
	1    5000 5050
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R8
U 1 1 5D9EA53B
P 4650 4050
F 0 "R8" V 4443 4050 50  0000 C CNN
F 1 "13k" V 4534 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4580 4050 50  0001 C CNN
F 3 "~" H 4650 4050 50  0001 C CNN
F 4 "x" H 4650 4050 50  0001 C CNN "onderdelen 5mar20"
	1    4650 4050
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R9
U 1 1 5D9EA953
P 4650 4350
F 0 "R9" V 4443 4350 50  0000 C CNN
F 1 "390k" V 4534 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4580 4350 50  0001 C CNN
F 3 "~" H 4650 4350 50  0001 C CNN
F 4 "x" H 4650 4350 50  0001 C CNN "onderdelen 5mar20"
	1    4650 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 4350 4800 4050
Wire Wire Line
	4800 4050 4900 4050
Connection ~ 4800 4050
Wire Wire Line
	4500 4050 4400 4050
Wire Wire Line
	4500 4350 4500 4050
Connection ~ 4500 4050
$Comp
L SemiparamEQ1-rescue:GND-power #PWR02
U 1 1 5D9FAAE4
P 4350 4350
F 0 "#PWR02" H 4350 4100 50  0001 C CNN
F 1 "GND" H 4355 4177 50  0000 C CNN
F 2 "" H 4350 4350 50  0001 C CNN
F 3 "" H 4350 4350 50  0001 C CNN
	1    4350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4350 4350 4250
$Comp
L SemiparamEQ1-rescue:C-Device C2
U 1 1 5DA00362
P 4100 3550
F 0 "C2" V 3848 3550 50  0000 C CNN
F 1 "1000p" V 3939 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4138 3400 50  0001 C CNN
F 3 "~" H 4100 3550 50  0001 C CNN
F 4 "x" H 4100 3550 50  0001 C CNN "onderdelen 5mar20"
	1    4100 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 4050 4400 3550
Wire Wire Line
	4400 3550 4250 3550
Connection ~ 4400 4050
Wire Wire Line
	4400 4050 4350 4050
Wire Wire Line
	3750 4150 3750 3550
Wire Wire Line
	3750 3550 3950 3550
Wire Wire Line
	3150 4150 3750 4150
Wire Wire Line
	2850 4150 2250 4150
Wire Wire Line
	2250 4150 2250 4650
Connection ~ 2250 4650
Connection ~ 6300 5000
Wire Wire Line
	6300 5000 6300 4600
Text GLabel 1600 4500 2    50   Input ~ 0
BPin
Wire Wire Line
	2000 5100 2000 5400
Text Notes 6700 2250 0    50   ~ 10
SUMMING SIGNAL + BP
Connection ~ 7350 2750
Wire Wire Line
	7350 2750 7500 2750
Wire Wire Line
	6300 5000 6650 5000
Text Notes 2100 3900 0    50   ~ 0
Gain = 3, Q = 2\nFc = 250Hz at Digipots = 0x00\nFc = 4kHz at Digipots = 0xFF
$Sheet
S 4800 6000 1050 900 
U 5DC0D667
F0 "Frequency Setting" 50
F1 "Frequency.sch" 50
F2 "Digipot_H0" O L 4800 6250 50 
F3 "Digipot_W0" O R 5850 6350 50 
F4 "Digipot_H1" O L 4800 6450 50 
F5 "Digipot_W1" O R 5850 6600 50 
F6 "V_ampl" I L 4800 6800 50 
F7 "VRAW" I R 5850 6150 50 
F8 "VinXmega" I R 5850 6050 50 
$EndSheet
Wire Wire Line
	5500 3950 5800 3950
Text GLabel 5800 3950 2    50   Output ~ 0
Digipot_W1
Wire Wire Line
	3100 5700 6300 5700
Wire Wire Line
	6300 4200 6250 4200
Connection ~ 6300 4600
Wire Wire Line
	6300 4600 6300 4200
Text GLabel 6250 4200 0    50   Input ~ 0
Digipot_H1
Wire Wire Line
	4000 4950 3700 4950
Text GLabel 3700 4950 0    50   Input ~ 0
Digipot_W0
Wire Wire Line
	3250 5200 3050 5200
Text GLabel 3250 5200 2    50   Output ~ 0
Digipot_H0
Text GLabel 4550 6250 0    50   Input ~ 0
Digipot_H0
Text GLabel 4600 6450 0    50   Input ~ 0
Digipot_H1
Text GLabel 6050 6350 2    50   Output ~ 0
Digipot_W0
Text GLabel 6050 6600 2    50   Output ~ 0
Digipot_W1
Wire Wire Line
	6050 6600 5850 6600
Wire Wire Line
	6050 6350 5850 6350
Wire Wire Line
	4600 6450 4800 6450
$Sheet
S 1450 1100 1350 700 
U 5DCEAD28
F0 "I/O & Supply" 50
F1 "IO.sch" 50
F2 "V_out" I L 1450 1400 50 
F3 "V_in" O R 2800 1650 50 
F4 "VinXmega" I L 1450 1650 50 
F5 "VRAW" I L 1450 1750 50 
F6 "SAMin" I L 1450 1200 50 
F7 "SAMout" I R 2800 1250 50 
$EndSheet
Text GLabel 10800 2950 2    50   Output ~ 0
V_out
Text Label 6300 4900 0    50   ~ 0
BP_out
Connection ~ 10200 2950
$Comp
L SemiparamEQ1-rescue:R-Device R15
U 1 1 5DD7DA59
P 8550 3200
F 0 "R15" V 8343 3200 50  0000 C CNN
F 1 "0R_BPin" V 8434 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8480 3200 50  0001 C CNN
F 3 "~" H 8550 3200 50  0001 C CNN
F 4 "x" H 8550 3200 50  0001 C CNN "onderdelen 5mar20"
	1    8550 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 3200 8700 3200
$Comp
L SemiparamEQ1-rescue:R-Device R12
U 1 1 5DD82B9C
P 6850 5000
F 0 "R12" V 6643 5000 50  0000 C CNN
F 1 "0R_BPout" V 6734 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6780 5000 50  0001 C CNN
F 3 "~" H 6850 5000 50  0001 C CNN
F 4 "x" H 6850 5000 50  0001 C CNN "onderdelen 5mar20"
	1    6850 5000
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:TestPoint_Probe-Connector TP2
U 1 1 5DD8366B
P 6650 4550
F 0 "TP2" H 6803 4651 50  0000 L CNN
F 1 "BPoutAC" H 6803 4560 50  0000 L CNN
F 2 "Codesign:TestPoint_Pad_3.4x4.7mm" H 6850 4550 50  0001 C CNN
F 3 "~" H 6850 4550 50  0001 C CNN
F 4 "x" H 6650 4550 50  0001 C CNN "onderdelen 5mar20"
	1    6650 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4550 6650 5000
Connection ~ 6650 5000
Wire Wire Line
	6650 5000 6700 5000
$Comp
L SemiparamEQ1-rescue:Conn_01x02-Connector_Generic J1
U 1 1 5DD88DFB
P 650 5100
F 0 "J1" H 568 5317 50  0000 C CNN
F 1 "BPin" H 568 5226 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x02_P1.27mm_Vertical" H 650 5100 50  0001 C CNN
F 3 "~" H 650 5100 50  0001 C CNN
F 4 "x" H 650 5100 50  0001 C CNN "onderdelen 5mar20"
	1    650  5100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	850  5200 850  5100
Wire Wire Line
	850  5100 900  5100
Connection ~ 850  5100
$Comp
L SemiparamEQ1-rescue:C-Device C1
U 1 1 5DDBA5EA
P 1050 5100
F 0 "C1" V 798 5100 50  0000 C CNN
F 1 "1u" V 889 5100 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W6.0mm_P5.00mm" H 1088 4950 50  0001 C CNN
F 3 "~" H 1050 5100 50  0001 C CNN
F 4 "x" H 1050 5100 50  0001 C CNN "onderdelen 5mar20"
	1    1050 5100
	0    1    1    0   
$EndComp
Text GLabel 3000 1650 2    50   Output ~ 0
V_in
Text GLabel 4550 6800 0    50   Output ~ 0
V_ampl
Text GLabel 1200 1400 0    50   Output ~ 0
V_out
Wire Wire Line
	1200 1400 1450 1400
Wire Wire Line
	2800 1650 3000 1650
Text GLabel 7050 5400 0    50   Output ~ 0
V_ampl
Wire Wire Line
	2000 5100 2250 5100
Connection ~ 2000 5100
Connection ~ 2250 5100
Text Notes 2100 3600 0    50   ~ 10
BANDPASS
Wire Wire Line
	1550 4500 1600 4500
Wire Wire Line
	1550 4500 1550 5100
$Comp
L SemiparamEQ1-rescue:TestPoint_Probe-Connector TP1
U 1 1 5D9801A1
P 3750 3100
F 0 "TP1" H 3902 3201 50  0000 L CNN
F 1 "Int2AC" H 3902 3110 50  0000 L CNN
F 2 "Codesign:TestPoint_Pad_3.4x4.7mm" H 3950 3100 50  0001 C CNN
F 3 "~" H 3950 3100 50  0001 C CNN
F 4 "x" H 3750 3100 50  0001 C CNN "onderdelen 5mar20"
	1    3750 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3100 3750 3550
Connection ~ 3750 3550
$Comp
L SemiparamEQ1-rescue:LF412CD-Codesign U2
U 1 1 5D992C99
P 5950 5150
F 0 "U2" H 5950 4783 50  0000 C CNN
F 1 "LF412CD" H 5950 4874 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5950 5150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lf412.pdf" H 5950 5150 50  0001 C CNN
F 4 "x" H 5950 5150 50  0001 C CNN "onderdelen 5mar20"
	1    5950 5150
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LF412CD-Codesign U2
U 2 1 5D993E1D
P 4050 4150
F 0 "U2" H 4050 3783 50  0000 C CNN
F 1 "LF412CD" H 4050 3874 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4050 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lf412.pdf" H 4050 4150 50  0001 C CNN
F 4 "x" H 4050 4150 50  0001 C CNN "onderdelen 5mar20"
	2    4050 4150
	-1   0    0    1   
$EndComp
Connection ~ 3750 4150
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U4
U 1 1 5D9E4DAA
P 2650 5200
F 0 "U4" H 2650 4833 50  0000 C CNN
F 1 "LM833" H 2650 4924 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 2650 5200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 2650 5200 50  0001 C CNN
F 4 "x" H 2650 5200 50  0001 C CNN "onderdelen 5mar20"
	1    2650 5200
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U1
U 2 1 5D9E7592
P 4300 5050
F 0 "U1" H 4300 5417 50  0000 C CNN
F 1 "LM833" H 4300 5326 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 4300 5050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 4300 5050 50  0001 C CNN
F 4 "x" H 4300 5050 50  0001 C CNN "onderdelen 5mar20"
	2    4300 5050
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U1
U 1 1 5D9E80DF
P 5200 4050
F 0 "U1" H 5200 4417 50  0000 C CNN
F 1 "LM833" H 5200 4326 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 5200 4050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 5200 4050 50  0001 C CNN
F 4 "x" H 5200 4050 50  0001 C CNN "onderdelen 5mar20"
	1    5200 4050
	-1   0    0    -1  
$EndComp
Connection ~ 4900 4050
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U3
U 1 1 5D9E88D2
P 7950 2850
F 0 "U3" H 7950 2483 50  0000 C CNN
F 1 "LM833" H 7950 2574 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 7950 2850 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 7950 2850 50  0001 C CNN
F 4 "x" H 7950 2850 50  0001 C CNN "onderdelen 5mar20"
	1    7950 2850
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U3
U 2 1 5D9E8FAD
P 9800 2950
F 0 "U3" H 9800 2583 50  0000 C CNN
F 1 "LM833" H 9800 2674 50  0000 C CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 9800 2950 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 9800 2950 50  0001 C CNN
F 4 "x" H 9800 2950 50  0001 C CNN "onderdelen 5mar20"
	2    9800 2950
	1    0    0    1   
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U3
U 3 1 5DA0780C
P 5250 1400
F 0 "U3" H 5208 1446 50  0000 L CNN
F 1 "LM833" H 5208 1355 50  0000 L CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 5250 1400 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 5250 1400 50  0001 C CNN
F 4 "x" H 5250 1400 50  0001 C CNN "onderdelen 5mar20"
	3    5250 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR08
U 1 1 5DA07812
P 4700 1400
F 0 "#PWR08" H 4700 1150 50  0001 C CNN
F 1 "GND" H 4705 1227 50  0000 C CNN
F 2 "" H 4700 1400 50  0001 C CNN
F 3 "" H 4700 1400 50  0001 C CNN
	1    4700 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C6
U 1 1 5DA07818
P 5000 1100
F 0 "C6" V 4748 1100 50  0000 C CNN
F 1 "100n" V 4839 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 950 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
F 4 "x" H 5000 1100 50  0001 C CNN "onderdelen 5mar20"
	1    5000 1100
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C7
U 1 1 5DA0781E
P 5000 1700
F 0 "C7" V 4748 1700 50  0000 C CNN
F 1 "100n" V 4839 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 1550 50  0001 C CNN
F 3 "~" H 5000 1700 50  0001 C CNN
F 4 "x" H 5000 1700 50  0001 C CNN "onderdelen 5mar20"
	1    5000 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 1400 4700 1400
Connection ~ 4850 1400
Wire Wire Line
	4850 1100 4850 1400
Wire Wire Line
	4850 1400 4850 1700
Wire Wire Line
	5150 1050 5150 1100
Connection ~ 5150 1100
Wire Wire Line
	5150 1750 5150 1700
Connection ~ 5150 1700
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U1
U 3 1 5DA0D6B5
P 4400 1400
F 0 "U1" H 4358 1446 50  0000 L CNN
F 1 "LM833" H 4358 1355 50  0000 L CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 4400 1400 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 4400 1400 50  0001 C CNN
F 4 "x" H 4400 1400 50  0001 C CNN "onderdelen 5mar20"
	3    4400 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR04
U 1 1 5DA0D6BB
P 3850 1400
F 0 "#PWR04" H 3850 1150 50  0001 C CNN
F 1 "GND" H 3855 1227 50  0000 C CNN
F 2 "" H 3850 1400 50  0001 C CNN
F 3 "" H 3850 1400 50  0001 C CNN
	1    3850 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C4
U 1 1 5DA0D6C1
P 4150 1100
F 0 "C4" V 3898 1100 50  0000 C CNN
F 1 "100n" V 3989 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4188 950 50  0001 C CNN
F 3 "~" H 4150 1100 50  0001 C CNN
F 4 "x" H 4150 1100 50  0001 C CNN "onderdelen 5mar20"
	1    4150 1100
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C5
U 1 1 5DA0D6C7
P 4150 1700
F 0 "C5" V 3898 1700 50  0000 C CNN
F 1 "100n" V 3989 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4188 1550 50  0001 C CNN
F 3 "~" H 4150 1700 50  0001 C CNN
F 4 "x" H 4150 1700 50  0001 C CNN "onderdelen 5mar20"
	1    4150 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 1400 3850 1400
Connection ~ 4000 1400
Wire Wire Line
	4000 1100 4000 1400
Wire Wire Line
	4000 1400 4000 1700
Wire Wire Line
	4300 1050 4300 1100
Connection ~ 4300 1100
Wire Wire Line
	4300 1750 4300 1700
Connection ~ 4300 1700
Wire Wire Line
	4800 6250 4550 6250
Wire Wire Line
	1200 5100 1550 5100
Wire Wire Line
	4800 6800 4550 6800
Text Notes 6550 5300 0    50   ~ 0
To Freq sheet for Xmega ADC
Text GLabel 6050 6050 2    50   Input ~ 0
VinXmega
Wire Wire Line
	5850 6050 6050 6050
Text GLabel 6050 6150 2    50   Input ~ 0
VRAW
Wire Wire Line
	6050 6150 5850 6150
Text GLabel 1200 1650 0    50   Output ~ 0
VinXmega
Text GLabel 1200 1750 0    50   Output ~ 0
VRAW
Wire Wire Line
	1450 1750 1200 1750
Wire Wire Line
	1200 1650 1450 1650
$Comp
L power:+12V #PWR0104
U 1 1 5E677EF2
P 5150 1050
F 0 "#PWR0104" H 5150 900 50  0001 C CNN
F 1 "+12V" H 5165 1223 50  0000 C CNN
F 2 "" H 5150 1050 50  0001 C CNN
F 3 "" H 5150 1050 50  0001 C CNN
	1    5150 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0105
U 1 1 5E6780FD
P 4300 1050
F 0 "#PWR0105" H 4300 900 50  0001 C CNN
F 1 "+12V" H 4315 1223 50  0000 C CNN
F 2 "" H 4300 1050 50  0001 C CNN
F 3 "" H 4300 1050 50  0001 C CNN
	1    4300 1050
	1    0    0    -1  
$EndComp
$Comp
L power:-12V #PWR0108
U 1 1 5E678DA0
P 5150 1750
F 0 "#PWR0108" H 5150 1850 50  0001 C CNN
F 1 "-12V" H 5165 1923 50  0000 C CNN
F 2 "" H 5150 1750 50  0001 C CNN
F 3 "" H 5150 1750 50  0001 C CNN
	1    5150 1750
	-1   0    0    1   
$EndComp
$Comp
L power:-12V #PWR0109
U 1 1 5E6791BD
P 4300 1750
F 0 "#PWR0109" H 4300 1850 50  0001 C CNN
F 1 "-12V" H 4315 1923 50  0000 C CNN
F 2 "" H 4300 1750 50  0001 C CNN
F 3 "" H 4300 1750 50  0001 C CNN
	1    4300 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	10200 2950 10800 2950
Text Notes 9000 4750 0    50   ~ 0
Switch 1 boost / 2 bypass/ 3 cut
$Comp
L SemiparamEQ1-rescue:GND-power #PWR?
U 1 1 5E73C253
P 9700 4250
AR Path="/5DB4A226/5E73C253" Ref="#PWR?"  Part="1" 
AR Path="/5E73C253" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 9700 4000 50  0001 C CNN
F 1 "GND" H 9705 4077 50  0000 C CNN
F 2 "" H 9700 4250 50  0001 C CNN
F 3 "" H 9700 4250 50  0001 C CNN
	1    9700 4250
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:R-Device R?
U 1 1 5E73C25A
P 9700 4050
AR Path="/5DB4A226/5E73C25A" Ref="R?"  Part="1" 
AR Path="/5E73C25A" Ref="R37"  Part="1" 
F 0 "R37" H 9770 4096 50  0000 L CNN
F 1 "100k" H 9770 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9630 4050 50  0001 C CNN
F 3 "~" H 9700 4050 50  0001 C CNN
F 4 "x" H 9700 4050 50  0001 C CNN "onderdelen 5mar20"
	1    9700 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 4200 9700 4250
Wire Wire Line
	9700 3850 9700 3900
Text Label 9700 3850 2    50   ~ 0
Bypass
$Comp
L SemiparamEQ1-rescue:SW_Rotary1x3-Codesign SW?
U 1 1 5E73C264
P 8850 4400
AR Path="/5DB4A226/5E73C264" Ref="SW?"  Part="1" 
AR Path="/5E73C264" Ref="SW1"  Part="1" 
F 0 "SW1" V 8800 4690 50  0000 C CNN
F 1 "SW_Rotary1x3" V 8700 4800 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-5-8_PD5.08" H 8650 4700 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/C200/DS-Serie%23LOR.pdf" H 8650 4700 50  0001 C CNN
F 4 "x" H 8850 4400 50  0001 C CNN "onderdelen 5mar20"
	1    8850 4400
	0    1    -1   0   
$EndComp
Wire Wire Line
	8850 3850 9700 3850
$Sheet
S 7200 4700 1100 800 
U 5DB4A226
F0 "Amplitude Setting" 50
F1 "Amplitude.sch" 50
F2 "Rx_a" O L 7200 5000 50 
F3 "V_ampl" I L 7200 5400 50 
F4 "Rx_b" I R 8300 5300 50 
$EndSheet
Wire Wire Line
	7050 5400 7200 5400
Wire Wire Line
	7200 5000 7000 5000
$Comp
L power:-12V #PWR0107
U 1 1 5E678AE2
P 6050 1750
F 0 "#PWR0107" H 6050 1850 50  0001 C CNN
F 1 "-12V" H 6065 1923 50  0000 C CNN
F 2 "" H 6050 1750 50  0001 C CNN
F 3 "" H 6050 1750 50  0001 C CNN
	1    6050 1750
	-1   0    0    1   
$EndComp
$Comp
L power:-12V #PWR0106
U 1 1 5E6787C5
P 6950 1800
F 0 "#PWR0106" H 6950 1900 50  0001 C CNN
F 1 "-12V" H 6965 1973 50  0000 C CNN
F 2 "" H 6950 1800 50  0001 C CNN
F 3 "" H 6950 1800 50  0001 C CNN
	1    6950 1800
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0103
U 1 1 5E677B57
P 6050 1050
F 0 "#PWR0103" H 6050 900 50  0001 C CNN
F 1 "+12V" H 6065 1223 50  0000 C CNN
F 2 "" H 6050 1050 50  0001 C CNN
F 3 "" H 6050 1050 50  0001 C CNN
	1    6050 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0102
U 1 1 5E677667
P 6950 1100
F 0 "#PWR0102" H 6950 950 50  0001 C CNN
F 1 "+12V" H 6965 1273 50  0000 C CNN
F 2 "" H 6950 1100 50  0001 C CNN
F 3 "" H 6950 1100 50  0001 C CNN
	1    6950 1100
	1    0    0    -1  
$EndComp
Connection ~ 6950 1750
Connection ~ 6050 1700
Wire Wire Line
	6050 1750 6050 1700
$Comp
L SemiparamEQ1-rescue:LF412CD-Codesign U2
U 3 1 5D99A290
P 7050 1450
F 0 "U2" H 7008 1496 50  0000 L CNN
F 1 "LF412CD" H 7008 1405 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7050 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lf412.pdf" H 7050 1450 50  0001 C CNN
F 4 "x" H 7050 1450 50  0001 C CNN "onderdelen 5mar20"
	3    7050 1450
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C11
U 1 1 5D99CDBE
P 6800 1750
F 0 "C11" V 6548 1750 50  0000 C CNN
F 1 "100n" V 6639 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6838 1600 50  0001 C CNN
F 3 "~" H 6800 1750 50  0001 C CNN
F 4 "x" H 6800 1750 50  0001 C CNN "onderdelen 5mar20"
	1    6800 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 1800 6950 1750
Connection ~ 6950 1150
Connection ~ 6050 1100
Wire Wire Line
	6050 1050 6050 1100
Wire Wire Line
	5750 1400 5750 1700
Wire Wire Line
	5750 1100 5750 1400
Connection ~ 5750 1400
Wire Wire Line
	5750 1400 5600 1400
$Comp
L SemiparamEQ1-rescue:C-Device C9
U 1 1 5D9EB8A0
P 5900 1700
F 0 "C9" V 5648 1700 50  0000 C CNN
F 1 "100n" V 5739 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5938 1550 50  0001 C CNN
F 3 "~" H 5900 1700 50  0001 C CNN
F 4 "x" H 5900 1700 50  0001 C CNN "onderdelen 5mar20"
	1    5900 1700
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:C-Device C8
U 1 1 5D9EB89A
P 5900 1100
F 0 "C8" V 5648 1100 50  0000 C CNN
F 1 "100n" V 5739 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5938 950 50  0001 C CNN
F 3 "~" H 5900 1100 50  0001 C CNN
F 4 "x" H 5900 1100 50  0001 C CNN "onderdelen 5mar20"
	1    5900 1100
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR011
U 1 1 5D9EB894
P 5600 1400
F 0 "#PWR011" H 5600 1150 50  0001 C CNN
F 1 "GND" H 5605 1227 50  0000 C CNN
F 2 "" H 5600 1400 50  0001 C CNN
F 3 "" H 5600 1400 50  0001 C CNN
	1    5600 1400
	1    0    0    -1  
$EndComp
$Comp
L SemiparamEQ1-rescue:LM833-Codesign U4
U 3 1 5D9EA323
P 6150 1400
F 0 "U4" H 6108 1446 50  0000 L CNN
F 1 "LM833" H 6108 1355 50  0000 L CNN
F 2 "Package_SO:SO-8_3.9x4.9mm_P1.27mm" H 6150 1400 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/STMicroelectronics-LM833DT_C127074.pdf" H 6150 1400 50  0001 C CNN
F 4 "x" H 6150 1400 50  0001 C CNN "onderdelen 5mar20"
	3    6150 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1450 6650 1750
Wire Wire Line
	6650 1150 6650 1450
Wire Wire Line
	6950 1100 6950 1150
Connection ~ 6650 1450
Wire Wire Line
	6650 1450 6500 1450
$Comp
L SemiparamEQ1-rescue:C-Device C10
U 1 1 5D99C3C1
P 6800 1150
F 0 "C10" V 6548 1150 50  0000 C CNN
F 1 "100n" V 6639 1150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6838 1000 50  0001 C CNN
F 3 "~" H 6800 1150 50  0001 C CNN
F 4 "x" H 6800 1150 50  0001 C CNN "onderdelen 5mar20"
	1    6800 1150
	0    1    1    0   
$EndComp
$Comp
L SemiparamEQ1-rescue:GND-power #PWR015
U 1 1 5D99BC06
P 6500 1450
F 0 "#PWR015" H 6500 1200 50  0001 C CNN
F 1 "GND" H 6505 1277 50  0000 C CNN
F 2 "" H 6500 1450 50  0001 C CNN
F 3 "" H 6500 1450 50  0001 C CNN
	1    6500 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3200 8350 3200
Wire Wire Line
	8450 3450 8350 3450
Wire Wire Line
	8350 3450 8350 3200
Connection ~ 8350 3200
$Comp
L Jumper:Jumper_3_Open JP4
U 1 1 5E8692AD
P 8850 5300
F 0 "JP4" H 8850 5524 50  0000 C CNN
F 1 "Jumper_3_Open" H 8850 5450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8850 5300 50  0001 C CNN
F 3 "~" H 8850 5300 50  0001 C CNN
F 4 "x" H 8850 5300 50  0001 C CNN "onderdelen 5mar20"
	1    8850 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	7350 2750 7350 3650
Wire Wire Line
	8600 5300 8300 5300
Wire Wire Line
	9100 5300 9450 5300
Wire Wire Line
	8850 4000 8850 3850
Wire Wire Line
	9150 2850 9250 2850
Wire Wire Line
	8750 4000 8750 3650
Wire Wire Line
	8750 3650 7350 3650
Wire Wire Line
	8950 4000 8950 3650
Wire Wire Line
	8950 3650 9250 3650
Wire Wire Line
	9250 3650 9250 2850
Connection ~ 9250 2850
Wire Wire Line
	9250 2850 9400 2850
Wire Wire Line
	8850 5150 8850 4900
Text GLabel 8450 3450 2    50   Input ~ 0
SAMin
Text GLabel 9450 5300 2    50   Input ~ 0
SAMout
Text GLabel 2900 1250 2    50   Input ~ 0
SAMout
Wire Wire Line
	2900 1250 2800 1250
Text GLabel 1200 1200 0    50   Input ~ 0
SAMin
Wire Wire Line
	1200 1200 1450 1200
Text Notes 1650 4800 0    50   ~ 0
Rg 21067
Text Notes 4800 4750 0    50   ~ 0
Rf 12581
Text Notes 4500 3700 0    50   ~ 0
Rf 12581
Text Notes 1800 5850 0    50   ~ 0
Rq 11161
$EndSCHEMATC
