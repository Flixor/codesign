/*
 * CodesignXmegaAnaloog.c
 *
 * Created: 29-4-2020 14:33:28
 * Author : fpostma
 */ 

#define F_CPU     2000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>

#include "stream.h"


/* adc result vars */
static volatile uint16_t res_freq;
static volatile uint16_t res_ampl;

/* Necessary delay to let printf() complete successfully. */
#define DELAY_US_PRINTF 1000.0

#define pRST	PIN0_bm
#define pCLK	PIN1_bm
#define pDQ		PIN2_bm



/*
 * Initialisation of the ADC settings.
*/
void init_adc(void)
{
	// Freq ADC: pin A0
	PORTA.DIRCLR     |= PIN0_bm;						//configure pin A0 as an input
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN0_gc ;			//set pin A0 as ADCA channel 0 input
	ADCA.CH0.CTRL    = ADC_CH_INPUTMODE_SINGLEENDED_gc;	//single-ended mode on channel 0
	ADCA.CH0.INTCTRL = ADC_CH_INTLVL_LO_gc;				//enable low-level interrupt on ADCA channel 0
	
	// Ampl ADC: pin A1
	PORTA.DIRCLR     |= PIN1_bm;						//configure pin A1 as an input
	ADCA.CH1.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc ;			//set pin A1 as ADCA channel 1 input
	ADCA.CH1.CTRL    = ADC_CH_INPUTMODE_SINGLEENDED_gc;	//single-ended mode on channel 1
	ADCA.CH1.INTCTRL = ADC_CH_INTLVL_LO_gc;				//enable low-level interrupt on ADCA channel 1

	
	//General ADCA configuration
	ADCA.REFCTRL     = ADC_REFSEL_INTVCC_gc;	//VCC (3V3) is reference
	ADCA.CTRLB       = ADC_RESOLUTION_12BIT_gc;	//12 bit resolution
	ADCA.PRESCALER   = ADC_PRESCALER_DIV512_gc;	//2000000/512 = 3906 Hz samplerate
	ADCA.CTRLA       = ADC_ENABLE_bm;			//enable ADCA
}



/*
 * Freq ADC interrupt routine
*/
ISR(ADCA_CH0_vect)
{
	/* read */
	res_freq = ADCA.CH0.RESL;              
	res_freq |= ADCA.CH0.RESH << 8;   

	/* scaling offset to match Vfreq */
	if (res_freq > 190) res_freq -= 190;
	else res_freq = 0;
	
	uint16_t r = res_freq;
	//res_freq += res_freq / 17; // == factor 1.059
	res_freq += r * 7 / 100; // == factor 1.07
	
	/* starts conversion again */
	ADCA.CH0.CTRL |= ADC_CH_START_bm;           
}

/*
 * Ampl ADC interrupt routine
*/
ISR(ADCA_CH1_vect)
{
	/* read */
	res_ampl = ADCA.CH1.RESL;
	res_ampl |= ADCA.CH1.RESH << 8;
	
	/* match Vampl */
	if (res_ampl > 195) res_ampl -= 195;
	else res_ampl = 0;

	uint16_t r = res_ampl;
	res_ampl += r * 3/5; // factor 1.66
	res_ampl += r * 3/50; 	
	
	/* starts conversion again */
	ADCA.CH1.CTRL |= ADC_CH_START_bm;           
}


/*
 * writes 17 wiper bits to digipot
*/
void set_digipot(uint16_t val){
	/* set RST high to be able to write*/
	PORTD.OUTSET = pRST;

	for (int i = 0; i < 17; i++){
		/*stack select bit*/
		if (i == 0){
			PORTD.OUTCLR = pDQ;
		}
		/*MSB to LSB*/
		else {
			if ((val >> (16 - i)) & 0x01)
				PORTD.OUTSET = pDQ;
			else
				PORTD.OUTCLR = pDQ;
		}
		/*clock in the data*/
		PORTD.OUTTGL = pCLK;
		_delay_us(1);
		PORTD.OUTTGL = pCLK;
	}
	_delay_us(1);
	
	/*RST low*/
	PORTD.OUTCLR = pRST;	
	_delay_us(10);
}

/* 
 * converts voltage reading to 16 bits writeable to digipot,
 * to whatever the closest digipot tap is
 * one tap = 4000 Hz / 256 taps = 15.625 Hz
 * (both bytes are the same)
 */
uint16_t V_to_dp(uint16_t volt){
	uint16_t result;
	float tap = volt / 15.625;
	if (tap > 1.0) tap -= 1;
	if (tap > 127.0) tap -= 1;
	if (tap > 254.0) tap = 254.0f;
	result = (uint8_t) tap;
	result |= ((uint8_t) tap) << 8;
	return result;
}


uint16_t diff_check(uint16_t *res, uint16_t *saved, uint16_t min_diff_mv){
	
	uint16_t diff = *res - *saved;
	
	/* if significant voltage change: return true */
	if (diff > min_diff_mv && diff < (65536 - min_diff_mv)){
		*saved = *res;	
		return 1;
	}
	/* else: do nothing, return false */
	else {
		return 0;
	}
} 


int main(void)
{
	cli();								//clear interrupt data, to be able to set it
	
	init_stream(F_CPU);					//initialise uart and printf via uart
	init_adc();							//initialise ADC
	
	PMIC.CTRL |= PMIC_LOLVLEN_bm;		//enable low-level interrupts
	
	ADCA.CH0.CTRL |= ADC_CH_START_bm;	//start ADCA channel 0 conversion
	ADCA.CH1.CTRL |= ADC_CH_START_bm;	//start ADCA channel 1 conversion
	
	sei();	
	
	/* set pins as output */
	PORTD.DIRSET = pRST | pCLK | pDQ;
	/* init all low */
	PORTD.OUTCLR = pRST | pCLK | pDQ;
	
	
	printf("Init\r\n");
	

	uint16_t freq_saved, ampl_saved;
		
    while (1) 
    {
		/* check significance of adc result, and communicate Vampl and Vfreq to uart */
		
		/* 30 mV diff for ampl */
		if (diff_check(&res_ampl, &ampl_saved, 30)){
			printf("a%dk", ampl_saved);
			//printf("ampl %d \r\n", ampl_saved);
		}
		_delay_us(DELAY_US_PRINTF * 5);
		
		/* 18 mV diff for freq */
		if (diff_check(&res_freq, &freq_saved, 18)){			
			/* set digipot */
			set_digipot(V_to_dp(freq_saved));
			_delay_us(DELAY_US_PRINTF * 50);
			
			printf("f%dk", freq_saved);
			//printf("freq %d \r\n", freq_saved);
		}		
		_delay_us(DELAY_US_PRINTF * 5);
		
    }
}

