/*
Flixor
May 2020
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "read_data.c"


int main(int argc, char **argv){

	bool neg, neg_peak;
	float peak, peak_freq, p_true, diff, newdiff;
	float saved_freqs[2];
	float *freq_ptr = saved_freqs;
	float saved_ampls[2];
	float *ampl_ptr = saved_ampls;
	float freq_local, ampl_local;
	diff = 1.0;

	/*
	args:
	- string	datafile
	*/
	if (argc != 2){
		fprintf(stderr, "Incorrect numbers of arguments.\n");
		return -1;
	}

	/* process arguments */
	FILE *data = fopen(argv[1], "r");

	/* open and replace output file */
	FILE *bw = fopen("bandwidth.tsv", "w");
	/* print header */
	fprintf(bw, "freq\tpeak and bandwidth\n");


	/* find abs peak ampl and corresponding freq */
	while (read_line(data, true, &freq_local, &ampl_local, &neg)){

		/* if freq < 100 Hz: skip */
		if (freq_local < 45) {
			continue;
		}
		/* if freq > 10kHz: output peak value */
		else if (freq_local > 10000){
			fprintf(bw, "%g\t%g\n", peak_freq, neg_peak?-peak:peak);
			printf("Hz\tdB\n%5.1f\t%4.2f\n", peak_freq, neg_peak?-peak:peak);
			break;
		}

		/* if abs ampl is highest so far: save as peak */
		if (ampl_local > peak){
			peak = ampl_local;
			peak_freq = freq_local;
			neg_peak = neg;
		}
	}
	/* close datafile so we can reread from the top */
	fclose(data);


	/* if peak is smaller than 3 dB: don't find -3 dB points */
	if (peak < 3){
		fclose(bw);
		return 1;
	}


	/* determine true -3 dB point */
	p_true = peak - 3;	
	
	/* reopen datafile so we can reread from the top */
	data = fopen(argv[1], "r");

	/* find actual points closest to -3 dB */
	while (read_line(data, true, &freq_local, &ampl_local, &neg)){

		/* if freq < 100 Hz or > 10kHz: skip */
		if (freq_local < 100) 
			continue;
		else if (freq_local > 10000)
			break;

		/* determine ampl diff with true -3 dB point */
		newdiff = fabs(ampl_local - p_true);

		/* if diff smallest so far: save freq */
		if (newdiff < diff){
			diff = newdiff;
			*freq_ptr = freq_local;
			*ampl_ptr = neg_peak?-ampl_local:ampl_local;
		}

		/* if peak reached: move freq ptr to second freq slot */
		if (fabs(ampl_local - peak) < 0.01)  {
			freq_ptr = &saved_freqs[1];
			ampl_ptr = &saved_ampls[1];
			diff = 1.0; /* reset diff measurement */
		}
	}

	/* at end of file: output the -3 dB points */
	fprintf(bw, "%g\t%g\n%g\t%g\n", saved_freqs[0], saved_ampls[0], saved_freqs[1], saved_ampls[1]);
	printf("%5.1f\t%4.2f\n%5.1f\t%4.2f\n", saved_freqs[0], saved_ampls[0], saved_freqs[1], saved_ampls[1]);

	/* print Q factor */
	float q = peak_freq / (saved_freqs[1] - saved_freqs[0]);
	printf("Q\n%5.3f\n", q);

	fclose(bw);
	fclose(data);

	return 0;
}
