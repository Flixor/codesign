reset session

### prep data
DATA = ARG1
set datafile separator "\t"

### prep style
set style line 1 lw 2 lc rgb "#80AA00B8"
set style line 2 lw 2 lc rgb "#502BB033"
set style line 3 lw 2 lc rgb "#70FF4F00"
set style line 4 lw 2 lc rgb "#900000FF"
set style line 5 lw 2 lc rgb "#70FF78DA"
set style line 6 lw 2 lc rgb "#70000000"
set style line 7 lw 2 lc rgb "#70FFAB11"
set style line 8 lw 2 lc rgb "#50D8D400"
set style increment user
set border linewidth 2
set term qt size 1200, 600 font "sans, 12"

### prep labeling
set key autotitle columnhead
set key left
set grid

set ylabel "Amplitude [dB]"
set ytics -18, 3, 18
set mytics 3

set xlabel "Frequency [Hz]"
set logscale x
set xtics 10, 10, 10000
set xtics format ""
set xtics add (10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000, 22000, 30000, 40000)
set xtics add ("10"10, "20" 20, "50" 50, "100" 100, "200" 200, "400" 400, "600" 600, "800" 800, "1k"1000, "2k" 2000, "4k"4000, "6k"6000, "8k"8000, "10k"10000, "15k"15000, "20k"20000, "22k"22000, "40k"40000)


### invoke bw.c as function with arg, to create bandwidth.tsv with peak and -3 dB points
system(sprintf("./bw \"%s\"", DATA))
stats 'bandwidth.tsv' using 1 nooutput name 'bw_'
if (bw_index_max > 0){
	Q = bw_median / (bw_max - bw_min);
	set label sprintf("Q-factor: %5.3f", Q) at 100,9 center
}


### final plot
set xrange [15:24000]
set yrange [-15:15]
plot DATA u 1:3 w li, 'bandwidth.tsv' u 1:2:(sprintf("%5.2f dB\n%6.1f Hz", $2, $1)) with labels point pt 7 offset 0,2



