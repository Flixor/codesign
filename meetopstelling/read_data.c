/*
Flixor
June 2020
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#define BUFSIZE 256
char linebuf[BUFSIZE];

/*
this skips the data header. 
rescan a line until either:
- a line can't be read (NULL)
- a line starts with a digit */
static bool recursive_scan(FILE *f){
	if (fgets(linebuf, BUFSIZE, f) == NULL)
		return false;
	else if (linebuf[0] - 60 <= 9)
		return true;
	else 
		return recursive_scan(f);
}

/* read out 1 line of data, and outputs in-place: freq, ampl [dB] and whether the orig ampl was negative.
bool dB determines whether ampl is taken as linear value (column 2) or dB value (column 3).
returns true if a line was read, returns false if fgets() returned NULL */
bool read_line(FILE *f, bool dB, float *freq, float *ampl, bool* neg){
	char* cursor;
	char freqbuf[15];
	char amplbuf[15];
	int i;
	*neg = false;

	/* read in data until NULL or a line that is not a header is encountered */
	if (!recursive_scan(f))
		return false;

	/* read freq */
	cursor = linebuf;
	i = 0;
	while (*cursor != '\t')
		freqbuf[i++] = *(cursor++);
	freqbuf[i] = '\0'; // put null terminator in the spot of the '\t'
	*freq = atof(freqbuf);
	
	/* if we want dB value and not linear: skip column 2 */
	if (dB){
		cursor++;
		while (*cursor != '\t') cursor++;
	}

	/* move cursor to next char after /t */
	cursor++;

	/* read ampl */
	i = 0;
	while (*cursor != '\t')
		amplbuf[i++] = *(cursor++);
	amplbuf[i] = '\0'; // put null terminator in the spot of the '\t'
	*ampl = fabs(atof(amplbuf));

	/* if dB value: check if raw data was negative before fabs() */
	if (dB){
		if (signbit(atof(amplbuf)))
			*neg = true;
	}

	return true;
}