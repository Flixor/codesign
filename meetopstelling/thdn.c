/*
Flixor
June 2020
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "read_data.c"


#define SIG_DIFFUSE_HZ 15


int main(int argc, char **argv){
	float freq_local, ampl_local;
	bool neg; // dummy argument for read_line
	float peak, peak_freq;
	float thdn_pwr = 0, sig_pwr = 0;
	int thdn_bin_cnt = 0;
	float snr;

	/*
	args:
	- string	datafile
	*/
	if (argc != 2){
		fprintf(stderr, "Incorrect numbers of arguments.\n");
		return -1;
	}

	/* process arguments */
	FILE *data = fopen(argv[1], "r");

	/* first find peak linear ampl and corresponding freq */
	while (read_line(data, false, &freq_local, &ampl_local, &neg)){

		/* if freq not in 20-22k Hz range: skip */
		if (freq_local < 20) continue;
		else if (freq_local > 22000) break;

		/* if abs ampl is highest so far: save as peak */
		if (ampl_local > peak){
			peak = ampl_local;
			peak_freq = freq_local;
		}
	}

	/* close and reopen datafile so we can reread from the top */
	fclose(data);
	data = fopen(argv[1], "r");

	/* determine SNR */
	while (read_line(data, false, &freq_local, &ampl_local, &neg)){

		/* if freq not in 20-22k Hz range: skip */
		// if (freq_local < 20) continue;
		// else if (freq_local > 22000) break;

		/* if bin is sig bin */
		if (freq_local > peak_freq - SIG_DIFFUSE_HZ && freq_local < peak_freq + SIG_DIFFUSE_HZ ){
			sig_pwr += ampl_local;
		}
		/* else: thd+n bin */
		else {
			thdn_pwr += ampl_local * ampl_local;
			thdn_bin_cnt++;
		}
	}

	/* get sig power by squaring summed sig bins ampl */
	sig_pwr *= sig_pwr;
	// sig_pwr = peak * peak;

	/* get THD+N power by averaging the squared ampls */
	thdn_pwr /= thdn_bin_cnt;

	/* get SNR from powers */
	// snr = 10 * log(sig_pwr / thdn_pwr);
	// printf("SNR ?? %g\n", snr);

	/* RMS */
	float sig_rms = sqrt(sig_pwr);
	float thdn_rms = sqrt(thdn_pwr);

	printf("THD+N %7.4f%%\n", thdn_rms / sig_rms * 100);


	FILE *out = fopen("thdn.tsv", "w");
	fprintf(out, "%7.4f\n", thdn_rms / sig_rms * 100);
	/* hacky double print so gnuplot stats command works */
	fprintf(out, "%7.4f\n", thdn_rms / sig_rms * 100);

	fclose(out);
	fclose(data);

	return 0;
}